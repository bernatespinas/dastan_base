###
### Spells -> Rituals so they are less boring?
###
### 01/03/20, 01:52. Encara al llit ;).
###

	I just thought about this... Like, a/the spell to ressurrect someone. I could make that spell require the player to place candles in a pentagon star formation or whatever. Then, place the body in the middle an stand in front of it or just be adjacent to it.

	I'd then need to check the map (passed as a parameter) positions... Like, in this case, find where exactly the body is, and then try to find the candles (taking in mind the map edges). If everything is OK, tiki tiki. If not, I could say nothing and the player would need to know what's up or try to find a book or something.

	Spells to duplicate food (adjacent free Tile available, or adjacent Tile with a random Item or something), mimic someone (a body sample ;), on the floor or attached to a voodoo doll or something), turn (NOPE, go, become (deepl)) invisible (no clothes).

	Oh, the ressurrection spell could go wrong if the star is inverted or something else is amiss (?) and ressurrect the body but controlled by a demon or something.

	Well, these are some random, nice ideas that might be hard to implement. But instead of what I originally thought ¿of? (cast fireballs other stolen things from other games), these sound more interesting and difficult.

	UPDATE 01/03/20, 12:47. M'he llevat fa unes horetes, esmorzat llet d'avena.

	Ohhh, I could use the spell.requirements_map and the like to create drawings on books to illustrate how that spell has to be cast!
###


###
### Recipes? Boring. Books? Oh my.
###
### 29/02/20, 22:18. Al meu llit de casa el papa (Marina 23), després de sopar.
###

	Recipes are just so... Ugh. WoW has recipes, so of course I had to steal them. Boring. Next.

	They feel too gamey, I think. (Are they even functional in-game?) I think I feel the same about them ¿than with? workshops: to build whatever you build a workshop first. To build something you have to know its recipe. Okay.

	Why can't you guess something? I have some wood planks / PLANK_WOOD and nails, and I'd like to have a cabinet/closet/wardrobe (deepl.com). Oh, but I don't know its recipe, deeeeerp, I'm useless. If you've seen an item, can't you guess how to make it? Or at least make a crude recreation of it...

	And you aren't walking through a forest and suddenly find a recipe laying on the floor near a cave entrance or whatever. Finding Recipes in chests or something is a bit..., gamey.

	Well, books could be interesting... And they could contain different types of things: recipes, spells, history, locations, names, maps... And they feel more treasure-y than recipes. Poor Recipes.

###

###
### A sudden Corpse? Well bye Door! (Lame... -_-)
###
### 29/02/20, 17:52
###

	I went to test/see those new buldings that are generated using the High Fidelity Viewing Experience System (HiFiVES ;) (so lame die)), and I decided to kill an Orc that was in my way. Well, that Orc was on the same Tile as a Door, and when it turned into a Corpse, the Door vanished.

	I thought about how lame this one-item-per-tile thing can be, but then I realized that the most annoying case is actually this one: when a Creature turns into a Corpse. Because there can be a Creature and an Item in the same Tile, and then it's the Creature that turns into a Corpse, an Item.

	When I cut a tree (say), the tree Item vanishes and in its place appears a wood Item. But when a Creature dies...

	Could Corpses be Creatures...? But then no one could step on them... Jump them, maybe?

###


###
### Price -> Not fixed, depends on base prices?
###
### 28/02/20, 11:55
###

	Instead of saying that a BED_WOOD costs 250, I could say it costs Price::Low(Material::Wood) or something, and then it would be assigned a cost in each shop/village/nation depending ¿on? the amount of Wood available? Umm...

###

### The enum ¿mess? ###

enum Item {Food, Equipment, Furniture...}... Every time I create a new type of Item, I have to add it there. And then, Furniture, for example, is an enum too: enum Furniture {Bed, Storage, Computer...}.

I have to impl ToItem for every Item variant, and ToFurniture for every Furniture variant... I also have to impl ToItem for Item, with those matches.

Also, every struct has name: &'a str, icon: char, color: Color... Although not all of them have blocks_passage: bool (sometimes they always block passage, and sometimes they never do, so there's no need to store it).

Despite this, if they share some basic fields, maybe I should make a re-usable thingy?

I tried making a "general"/"generic" struct Entity that has a component: T, but I don't know how to define that T.

If I do T: Component, where Component is a trait, I guess I could make it have a fn blocks_passage(&self) -> bool, but what else?
Also, traits bring their own set of problems (wow such language): Sized thingies, trait Objects...

I could then use enums instead...
Or instead of a "general" Entity struct with a component, make each thingy have a BasicInfo struct... But then I'd have to impl a trait to access its thingies (name icon color) from the struct itself...

I want to be able to say "this will be an Item (any type of Item)" or "this will be Food". With traits I guess that could be done:
	items: Vec<Entity<Item<ItemComponent>>>, // any type of Item
	foods: Vec<Entity<Item<Food>>>,

Would items be a Vec of Items of the SAME ItemComponent? Or not? Huh.

How could I do that if component is an enum?

UPDATE: &dyn ItemComponent and Box<ItemComponent> don't work because "deeeeeerp ItemComponent is not implemented for &dyn ItemComponent and Box<&dyn ItemComponent> deeeeerp". -_- I knew traits would be annoying.

Also, in that blog entry/post/_ about a state machine in Rust, generics where used to achieve something similar, right? But it was simply <S>, with no bounds or whatever. So...

###


· Generators' codes... Should I use the actual name of the thing?
	· UPDATE 29/02/20, 22:16 I guess not... Wood table? Or Wooden table? But then, Stone table... Wood floor? Or parqué or whatever? Grass, FLOOR_GRASS. Thing_material. I think it makes sense and it's easier to guess.

· RecipeBook stores Recipes in Vecs... Doesn's a HashSet make more sense, since there shouldn't be duplicates?
	· Without an id or something, it would be more difficult to access a Recipe... Scrollable.pos is so convenient.

· I just put the icon_open, _closed, and color_ too. To know which ones to choose, I check blocks_passage. ¿Should I store the current/correct/_ ones to icon and color when calling toggle()?
	· When I create Passage_s in PassageGen, I already choose whether blocks_passage it's true or false, so I might as well choose the current icon and color...? But..., what if I make a mistake?

· Hmm, there are a couple of structs (and probably many more to come) that share some attributes: name, icon, color, blocks_passage, blocks_sight, Category... Maybe even drops (Furniture and other things can be deconstructed)... Should I put them in a separate struct and reuse it everywhere, like those Python components?
	· Right now, (almost) all of those attributes are pub... I can access them easily, but also modify them... If I put them in a component, maybe I could restrict modifications, but accessing them might be harder...
		Creature.basicInfo.name? Creature.basicInfo.get_name()?
	· Maybe I shouldn't change anything... Uh......
	· Hmm, I searched that topic on the Internet... If something might change, it would be mut. I don't feel like writing a getter and a setter for each field... The only place where I had to do this is with traits such as ToItem and ToCreature (¿and ToFurniture?).

· So many types of Item_s... And then so many types of Item::Furniture_s... Maybe traits can solve this...?
	· Instead of storing an enum Item in the map, I would have to store a Box<ToItem> or impl a generic or something? Hmm... I still don't know if it would be worth it.

· trait ToEquipment, with fn to_equipment, that extracts the inner Equipment?
	· Equipment, Food, Recipe... So many of them!
	· Would I also need ref_$inner, $mut_inner...? TOO MANY?!?!?!?

· Make fighter, social..., components? But not behind/inside an Option, they have to be there. Just to avoid having everything in Creature.

· Butcher could englobe tanning and others. Tailor, cloth and leather working. These could be displayed in the Details window.

· Change hungry functions' names.

· Cloth, leather and mail equipment have different weights.

· Remove some pieces of equipment to keep it simple? Like, just leave head,
	upper body, lower body, feet... And jewelry would be rings, necklace...
	Jungle set, wolf set, eagle set...
		Eh?

· When creating and placing monsters on the map, make them have items equipped that will be dropped when they are killed.

· Body component: body parts, if blood: can bleed, if wings: can fly...




### DONE ###

// TODO: Hmm, since I need to create Equipment, Food, Creature, etc..., here, I have to make all of their fields pub... Should I put seeds.rs inside base.rs?

· In order to be able to create thingies in seeds.rs, I had to make all the fields of these sutrcts public... Maybe if I imported things in another way that wouldn't be needed...
	· Like, put all the creatures seeds things in base.rs (or base/creatures.rs)?

· utils.rs -> Put categories' enums in mod categories, time thingies in mod time... That way, I can put use utils::categories::*; instead of importing everything (ew, no) or importing every thing manually.

· Split base.rs into creatures.rs, items.rs, containers.rs (inventory, equipment, recipebook, questlog...)...

Instead of having a pub const ENUM_LIST: [Enum; n] to have every enum ¿instance? listed, I could make a trait that has a method list() -> Vec<Self>...

· Creature, Creatures (HashMap), Terrain, Terrains... The only difference is the tiny 's' at the end... Rename them to ThingGen or something.
Animal vs Animals, Plant vs Plants... Names are too similar. Change Animals to AnimalGenerator or something like that... It should be like the trait (its name might change too).

// TODO Should I store this information here? Hmm... I could store it in the Biome instead, or somewhere else...
	// It doesn't make sense to store them here because it would depend on the biome, terrain, weather... Right?
		// Yes. Also, that way I don't have to store it in every plant.
// pub seasons: Vec<Season>,
