use std::collections::HashMap;

use nrustes::Direction;

type MapRequirements = HashMap<(Direction, u8), RequirementType>;

#[derive(Clone, Serialize, Deserialize)]
enum RequirementType {
	Creature(String),
	Corpse(String),
	Item(String),
	// EquipmentSlot(Slot),
	// Equipment(String),
}

// enum RequirementEquipment {}

#[derive(Clone, Serialize, Deserialize)]
pub struct Spell {
	name: String,
	
	description: String,

	/// Names of the Items that have to be present in the Inventory.
	requirements_inv: Option<Vec<String>>,
	/// Things that have to be around the player, on the Map.
	requirements_map: Option<MapRequirements>,
	// requirements_equ: Option<
}

// struct MapRequirement {

// }

// trait Cast {

// }

#[derive(Clone, Serialize, Deserialize)]
pub struct SpellBook {
	spells: Vec<Spell>,
}

impl Default for SpellBook {
	fn default() -> SpellBook {
		SpellBook {
			spells: Vec::new(),
		}
	}
}