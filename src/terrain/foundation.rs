use std::collections::HashMap;

use super::{ToTerrain, Terrain, Generator, TerrainTypeGenerator, Color};

const WALL: char = 'ú';
const FLOOR: char = 'ú';

#[derive(Clone, Serialize, Deserialize)]
pub struct Foundation {
	pub name: String,
	pub icon: char,
	pub bg: Color,
	pub fg: Color,

	pub blocks_passage: bool,
	// pub blocks_sight: bool,

	// There might be an Item dug in (?).
	// content: Option<Item>,
	// can_be_mined: bool,
}

impl ToTerrain for Foundation {
	fn to_terrain(self) -> Terrain {
		Terrain::Foundation(self)
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn bg(&self) -> Color {
		self.bg
	}

	fn fg(&self) -> Color {
		self.fg
	}

	fn blocks_passage(&self) -> bool {
		self.blocks_passage
	}
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum FoundationType {
	Floor(Material),
	Wall(Material),
	Void,
	Planet,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum Material {
	Wood,
	Stone,
	Ice,
	Dirt,
	Grass,
}

pub struct FoundationGen<'a> {
	hashmap: HashMap<FoundationType, Terrain>,

	str_to_type: HashMap<&'a str, FoundationType>,
}

impl<'a> FoundationGen<'a> {
	pub fn generate() -> FoundationGen<'a> {
		let mut foundation_gen = FoundationGen {
			hashmap: HashMap::new(),
			str_to_type: HashMap::new(),
		};

		foundation_gen.insert("FLOOR_DIRT", FoundationType::Floor(Material::Dirt), Foundation {
			name: "Dirt floor".to_string(),
			icon: FLOOR,
			bg: Color::DBrown,
			fg: Color::DGreen,

			blocks_passage: false,
			// blocks_sight: false,

		});

		foundation_gen.insert("WALL_DIRT", FoundationType::Wall(Material::Dirt), Foundation {
			name: "Dirt wall".to_string(),
			icon: WALL,
			bg: Color::DBrown,
			fg: Color::LBrown,

			blocks_passage: true,
			// blocks_sight: true,
		});

		foundation_gen.insert("FLOOR_GRASS", FoundationType::Floor(Material::Grass), Foundation {
			name: "Grass floor".to_string(),
			icon: FLOOR,
			bg: Color::LGreen,
			fg: Color::DGreen,

			blocks_passage: false,
			// blocks_sight: false,
		});

		foundation_gen.insert("WALL_GRASS", FoundationType::Wall(Material::Grass), Foundation {
			name: "Bush wall".to_string(),
			icon: WALL,
			bg: Color::DGreen,
			fg: Color::DGrey,

			blocks_passage: true,
			// blocks_sight: true,
		});

		foundation_gen.insert("FLOOR_ICE", FoundationType::Floor(Material::Ice), Foundation {
			name: "Ice floor".to_string(),
			icon: FLOOR,
			bg: Color::LBlue,
			fg: Color::LGrey,

			blocks_passage: false,
			// blocks_sight: false,
		});

		foundation_gen.insert("WALL_ICE", FoundationType::Wall(Material::Ice), Foundation {
			name: "Ice wall".to_string(),
			icon: WALL,
			bg: Color::DBlue,
			fg: Color::LGrey,

			blocks_passage: true,
			// blocks_sight: true,
		});

		foundation_gen.insert("FLOOR_STONE", FoundationType::Floor(Material::Stone), Foundation {
			name: "Stone floor".to_string(),
			icon: FLOOR,
			bg: Color::LGrey,
			fg: Color::DGrey,

			blocks_passage: false,
			// blocks_sight: false,
		});

		foundation_gen.insert("WALL_STONE", FoundationType::Wall(Material::Stone), Foundation {
			name: "Stone wall".to_string(),
			icon: WALL,
			bg: Color::DGrey,
			fg: Color::LGrey,

			blocks_passage: true,
			// blocks_sight: true,
		});

		foundation_gen.insert("FLOOR_WOOD", FoundationType::Floor(Material::Wood), Foundation {
			name: "Wood floor".to_string(),
			icon: FLOOR,
			bg: Color::DBrown,
			fg: Color::LBrown,

			blocks_passage: false,
			// blocks_sight: false,
		});

		foundation_gen.insert("WALL_WOOD", FoundationType::Wall(Material::Wood), Foundation {
			name: "Wood wall".to_string(),
			icon: WALL,
			bg: Color::LBrown,
			fg: Color::DBrown,

			blocks_passage: true,
			// blocks_sight: true,Terrrain
		});

		foundation_gen.insert("VOID", FoundationType::Void, Foundation {
			name: "Void".to_string(),
			icon: ':',
			bg: Color::Black,
			fg: Color::LGrey,
			
			blocks_passage: false,
		});

		foundation_gen.insert("PLANET_TILE", FoundationType::Planet, Foundation {
			name: "Planet tile".to_string(),
			icon: '#',
			bg: Color::Yellow,
			fg: Color::Red,

			blocks_passage: false,
		});

		foundation_gen
	}

	fn insert(&mut self, code: &'a str, foundation_type: FoundationType, foundation: Foundation) {
		self.hashmap.insert(foundation_type, Terrain::Foundation(foundation));
		self.str_to_type.insert(code, foundation_type);
	}
}

impl<'a> Generator for FoundationGen<'a> {
	type Output = Foundation;

	fn contains(&self, code: &str) -> bool {
		self.str_to_type.contains_key(code)
	}

	fn code(&self, code: &str) -> Result<String, String> {
		if self.str_to_type.contains_key(code) {
			Ok(code.to_string())
		}
		else {
			Err(format!("Code {} not valid in FoundationGen", code))
		}
	}

	fn get(&self, code: &str) -> Foundation {
		let type_material = self.str_to_type.get(code)
			.unwrap_or_else(|| panic!("Code {} not valid in FoundationGen", code))
		;

		self.hashmap.get(type_material)
			.map(|terrain| match terrain {
				Terrain::Foundation(foundation) => foundation,
				_ => panic!(),
			})
			.unwrap()
			.clone()
	}

	fn list(&self) -> Vec<&str> {
		let mut vec: Vec<&str> = Vec::with_capacity(self.hashmap.len());
		for key in self.str_to_type.keys() {
			vec.push(key);
		}
		vec.sort();
		vec
	}
}

impl<'a> TerrainTypeGenerator for FoundationGen<'a> {
	type TerrainType = FoundationType;

	fn name_to_type(&self, name: &str) -> Self::TerrainType {
		*self.str_to_type.get(name)
			.unwrap_or_else(|| panic!(
				"Name {} invalid in FoundationGen", name
			))
	}

	fn terrain(&self, foundation_type: &Self::TerrainType) -> &Terrain {
		self.hashmap.get(foundation_type).unwrap()
	}
}