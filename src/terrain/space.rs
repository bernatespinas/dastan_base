use std::collections::HashMap;

use super::{ToTerrain, Terrain, Generator, Color};

const DISTANT_STARS: char = ';';

#[derive(Clone, Serialize, Deserialize)]
pub struct Space<'a> {
	pub name: &'a str,
	pub icon: char,
	pub color: Color,

	pub topping: Color,
}

pub struct SpaceGen<'a> {
	hashmap: HashMap<&'a str, Space<'a>>,
}

impl<'a> SpaceGen<'a> {
	pub fn generate() -> SpaceGen<'a> {
		let mut hashmap: HashMap<&str, Space> = HashMap::with_capacity(2);

		hashmap.insert("Void", Space {
			name: "Void",
			icon: ' ',
			color: Color::Black,

			blocks_passage: false,
			// blocks_sight: false,

			topping: Color::Black,
		});

		hashmap.insert("Distant stars", Space {
			name: "Distant stars",
			icon: DISTANT_STARS,
			color: Color::Black,

			blocks_passage: false,
			// blocks_sight: false,

			topping: Color::LGrey,
		});

		SpaceGen {
			hashmap,
		}
	}
}