use std::collections::HashMap;

use super::{ToTerrain, Terrain, Generator, TerrainTypeGenerator, Color};

const WATER: char = 'w';

#[derive(Clone, Serialize, Deserialize)]
pub struct Liquid {
	pub name: String,
	pub icon: char,
	pub bg: Color,
	pub fg: Color,
}

impl ToTerrain for Liquid {
	fn to_terrain(self) -> Terrain {
		Terrain::Liquid(self)
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn bg(&self) -> Color {
		self.bg
	}

	fn fg(&self) -> Color {
		self.fg
	}

	fn blocks_passage(&self) -> bool {
		false
	}

	// fn blocks_sight(&self) -> bool;
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum LiquidType {
	SaltWater(Depth),
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum Depth {
	Shallow,
	Deep,
}

pub struct LiquidGen<'a> {
	hashmap: HashMap<LiquidType, Terrain>,

	str_to_type: HashMap<&'a str, LiquidType>,
}

impl<'a> LiquidGen<'a> {
	pub fn generate() -> LiquidGen<'a> {
		let mut liquid_gen = LiquidGen {
			hashmap: HashMap::new(),
			str_to_type: HashMap::new(),
		};

		liquid_gen.insert("Shallow salt water", LiquidType::SaltWater(Depth::Shallow), Liquid {
			name: "Shallow salt water".to_string(),
			icon: WATER,
			bg: Color::LBlue,
			fg: Color::LGrey,
		});

		liquid_gen.insert("Deep salt water", LiquidType::SaltWater(Depth::Deep), Liquid {
			name: "Deep salt water".to_string(),
			icon: WATER,
			bg: Color::DBlue,
			fg: Color::Black,
		});

		liquid_gen
	}

	fn insert(&mut self, name: &'a str, liquid_type: LiquidType, liquid: Liquid) {
		self.hashmap.insert(liquid_type, Terrain::Liquid(liquid));
		self.str_to_type.insert(name, liquid_type);
	}
}

impl<'a> Generator for LiquidGen<'a> {
	type Output = Liquid;

	fn contains(&self, code: &str) -> bool {
		self.str_to_type.contains_key(code)
	}

	fn code(&self, code: &str) -> Result<String, String> {
		if self.str_to_type.contains_key(code) {
			Ok(code.to_string())
		}
		else {
			Err(format!("Code {} not valid in LiquidGen", code))
		}
	}

	fn get(&self, code: &str) -> Liquid {
		let type_depth = self.str_to_type.get(code)
			.unwrap_or_else(|| panic!("Code {} not valid in LiquidGen", code))
		;

		self.hashmap.get(&type_depth)
			.map(|terrain| match terrain {
				Terrain::Liquid(liquid) => liquid,
				_ => panic!(),
			})
			.unwrap()
			.clone()
	}

	fn list(&self) -> Vec<&str> {
		let mut vec: Vec<&str> = Vec::with_capacity(self.hashmap.len());
		for key in self.str_to_type.keys() {
			vec.push(key);
		}
		vec.sort();
		vec
	}
}

impl<'a> TerrainTypeGenerator for LiquidGen<'a> {
	type TerrainType = LiquidType;

	fn name_to_type(&self, name: &str) -> Self::TerrainType {
		*self.str_to_type.get(name)
			.unwrap_or_else(|| panic!(
				"Name {} invalid in LiquidGen", name
			))
	}

	fn terrain(&self, liquid_type: &Self::TerrainType) -> &Terrain {
		&self.hashmap.get(liquid_type).as_ref().unwrap()
	}
}