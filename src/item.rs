use crate::{Creature, Color, Size, Weight, Generator};

pub mod corpse;

pub mod equipment;
use equipment::{Equipment, EquipmentGen};

pub mod food;
use food::{Food, FoodGen};

pub mod furniture;
use furniture::{Furniture, FurnitureGen};

pub mod plant;
use plant::{Plant, PlantGen};

pub mod recipe;

pub mod resource;
use resource::{Resource, ResourceGen};

pub mod seed;
use seed::Seed;

pub mod tool;
use tool::{Tool, ToolGen};

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum Material {
	Wood,
	Stone,
	Cloth,
	Iron,
	Leather,
// 	Mail,
}

impl std::fmt::Display for Material {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
		write!(f, "{:?}", self)
	}
}

#[derive(Debug, PartialEq, Copy, Clone, Serialize, Deserialize)]
pub enum Use {
	Eat,
	Equip,
	Dequip,
	Drop,
}

pub trait ToItem {
	fn to_item(self) -> Item;
	fn name(&self) -> &str;
	fn icon(&self) -> char;
	fn color(&self) -> Color;
	fn blocks_passage(&self) -> bool;
	fn size(&self) -> Size;
	fn weight(&self) -> Weight;
	fn price(&self) -> u16;
}

#[derive(Clone, Serialize, Deserialize)]
pub enum Item {
	Corpse(Box<corpse::Corpse>),
	Equipment(Box<Equipment>),
	Food(Box<Food>),
	Furniture(Box<Furniture>),
	Plant(Box<Plant>),
	Resource(Box<Resource>),
	Seed(Box<Seed>),
	Tool(Box<Tool>),
}

impl Item {
	// Returns true if self is a type of Item that has an Inventory inside.
	pub fn has_inv(&self) -> bool {
		match self {
			Item::Furniture(box Furniture::Storage(..))	=> true,
			Item::Corpse(corpse) => match corpse.creature {
				Creature::Animal(..)							=> false,
				Creature::Humanoid(..)							=> true,
				Creature::Spaceship(..)							=> true,
			},
			_													=> false,
		}
	}
}

impl ToItem for Item {
	fn to_item(self) -> Item {
		self
	}

	fn name(&self) -> &str {
		match self {
			Item::Food(food)			=> &food.name,
			Item::Equipment(equipment)	=> &equipment.name,
			Item::Furniture(furniture)	=> furniture.name(),
			Item::Corpse(corpse)		=> corpse.name(),
			Item::Plant(plant)			=> &plant.name,
			Item::Seed(seed)			=> &seed.name,
			Item::Resource(resource)	=> &resource.name,
			Item::Tool(tool)			=> tool.name(),
		}
	}

	fn icon(&self) -> char {
		match self {
			Item::Food(food)			=> food.icon,
			Item::Equipment(equipment)	=> equipment.icon,
			Item::Furniture(furniture)	=> furniture.icon(),
			Item::Corpse(corpse)		=> corpse.icon,
			Item::Plant(plant)			=> plant.icon,
			Item::Seed(seed)			=> seed.icon,
			Item::Resource(resource)	=> resource.icon,
			Item::Tool(tool)			=> tool.icon(),
		}
	}

	fn color(&self) -> Color {
		match self {
			Item::Food(food)			=> food.color,
			Item::Equipment(equipment)	=> equipment.color,
			Item::Furniture(furniture)	=> furniture.color(),
			Item::Corpse(corpse)		=> corpse.color,
			Item::Plant(plant)			=> plant.color,
			Item::Seed(seed)			=> seed.color,
			Item::Resource(resource)	=> resource.color,
			Item::Tool(tool)			=> tool.color(),
		}
	}

	fn blocks_passage(&self) -> bool {
		match self {
			Item::Food(..)				=> false,
			Item::Equipment(..)			=> false,
			Item::Furniture(furniture)	=> furniture.blocks_passage(),
			Item::Corpse(corpse)		=> corpse.blocks_passage,
			Item::Plant(plant)			=> plant.blocks_passage,
			Item::Seed(..)				=> false,
			Item::Resource(..)			=> false,
			Item::Tool(..)				=> false,
		}
	}

	fn size(&self) -> Size {
		match self {
			Item::Food(food)			=> food.size(),
			Item::Equipment(equipment)	=> equipment.size(),
			Item::Furniture(furniture)	=> furniture.size(),
			Item::Corpse(corpse)		=> corpse.size(),
			Item::Plant(plant)			=> plant.size(),
			Item::Seed(seed)			=> seed.size(),
			Item::Resource(resource)	=> resource.size(),
			Item::Tool(tool)			=> tool.size(),
		}
	}

	fn weight(&self) -> Weight {
		match self {
			Item::Food(food)			=> food.weight(),
			Item::Equipment(equipment)	=> equipment.weight(),
			Item::Furniture(furniture)	=> furniture.weight(),
			Item::Corpse(corpse)		=> corpse.weight(),
			Item::Plant(plant)			=> plant.weight(),
			Item::Seed(seed)			=> seed.weight(),
			Item::Resource(resource)	=> resource.weight(),
			Item::Tool(tool)			=> tool.weight(),
		}
	}

	fn price(&self) -> u16 {
		match self {
			Item::Food(food)			=> food.price,
			Item::Equipment(equipment)	=> equipment.price,
			Item::Furniture(furniture)	=> furniture.price(),
			Item::Corpse(corpse)		=> corpse.price,
			Item::Plant(plant)			=> plant.price,
			Item::Seed(seed)			=> seed.price,
			Item::Resource(resource)	=> resource.price,
			Item::Tool(tool)			=> tool.price(),
		}
	}
}

pub struct ItemBundle<'a> {
	pub equipment_gen: EquipmentGen<'a>,
	pub food_gen: FoodGen<'a>,
	pub furniture_gen: FurnitureGen<'a>,
	pub plant_gen: PlantGen<'a>,
	pub resource_gen: ResourceGen<'a>,
	pub tool_gen: ToolGen<'a>,
}

impl<'a> ItemBundle<'a> {
	pub fn new() -> ItemBundle<'a> {
		let equipment_gen = EquipmentGen::generate();
		let food_gen = FoodGen::generate();
		let furniture_gen = FurnitureGen::generate();
		let plant_gen = PlantGen::generate();
		let resource_gen = ResourceGen::generate();
		let tool_gen = ToolGen::generate();

		ItemBundle {
			equipment_gen,
			food_gen,
			furniture_gen,
			plant_gen,
			resource_gen,
			tool_gen,
		}
	}

	pub fn gen_item(&self, code: &str) -> Item {
		if self.equipment_gen.contains(code) {
			self.equipment_gen.get(code).to_item()
		}
		else if self.food_gen.contains(code) {
			self.food_gen.get(code).to_item()
		}
		else if self.furniture_gen.contains(code) {
			self.furniture_gen.get(code).to_item()
		}
		else if self.plant_gen.contains(code) {
			self.plant_gen.get(code).to_item()
		}
		else if self.resource_gen.contains(code) {
			self.resource_gen.get(code).to_item()
		}
		else if self.tool_gen.contains(code) {
			self.tool_gen.get(code).to_item()
		}
		else {
			panic!("code {} not found", code);
		}
	}

	pub fn gen_equipment(&self, code: &str) -> Equipment {
		self.equipment_gen.get(code)
	}

	pub fn gen_food(&self, code: &str) -> Food {
		self.food_gen.get(code)
	}

	pub fn gen_furniture(&self, code: &str) -> Furniture {
		self.furniture_gen.get(code)
	}

	pub fn gen_plant(&self, code: &str) -> Plant {
		self.plant_gen.get(code)
	}

	pub fn gen_resource(&self, code: &str) -> Resource {
		self.resource_gen.get(code)
	}
	
	pub fn gen_tool(&self, code: &str) -> Tool {
		self.tool_gen.get(code)
	}
}