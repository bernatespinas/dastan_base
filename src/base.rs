#![feature(box_syntax, box_patterns)]

#[macro_use]
extern crate serde_derive;

use std::collections::HashSet;
use std::fmt;
use std::cmp::Ordering;

pub use nrustes::Color;

use uuid::Uuid;

mod inventory;
pub use inventory::Inventory;

pub mod terrain;
pub use terrain::{
	Terrain, ToTerrain, TerrainBundle,
	Foundation, FoundationGen,
	Liquid, LiquidGen,
};

pub mod creature;
pub use creature::{
	Creature, ToCreature, CreatureBundle,
	QuestLog,
	body::{
		Body, OrganicBody, OrganicSlot, SpaceshipBody, SpaceshipSlot,
	},
	humanoid::{
		Humanoid, HumanoidGen, Race, Faction,
		},
	animal::{
		Animal, AnimalGen,
	},
	spaceship::{
		Spaceship, SpaceshipGen,
	},
};

pub mod item;
pub use item::{
	Item, ToItem, ItemBundle, Material,

	corpse::{
		Corpse,
	},

	equipment::{
		Equipment, EquipmentGen,
	},

	food::{
		Food, FoodGen, Effect,
	},

	furniture::{
		Furniture, ToFurniture, FurnitureGen,
		Computer, Generic, Workshop,
		Toggle, Lock,
	},

	plant::{
		Plant, PlantGen,
	},

	recipe::{
		Recipe, RecipeGen, RecipeBook, Profession,
	},

	resource::{
		Resource, ResourceGen,
	},

	seed::Seed,

	tool::{
		Tool, ToolGen,
	},
};

mod workshop;

mod spell;
pub use spell::{
	Spell, SpellBook,
};

pub mod time;

mod biome;
pub use biome::{Biome, BiomeGen};

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum Size {
	Tiny,
	Small,
	Medium,
	Big,
	Huge,
}

impl Size {
	fn num_value(&self) -> u8 {
		match self {
			Size::Tiny		=> 0,
			Size::Small		=> 1,
			Size::Medium	=> 2,
			Size::Big		=> 3,
			Size::Huge		=> 4,
		}
	}
}

impl fmt::Display for Size {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{:?}", self)
	}
}

impl PartialOrd for Size {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		Some(self.num_value().cmp(&other.num_value()))
	}
}

impl ListEnum for Size {
	fn list() -> Vec<Size> {
		vec!(
			Size::Tiny,
			Size::Small,
			Size::Medium,
			Size::Big,
			Size::Huge,
		)
	}
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum Weight {
	Light,
	Medium,
	Heavy,
	SuperHeavy,
}

impl Weight {
	fn num_value(&self) -> u8 {
		match self {
			Weight::Light		=> 0,
			Weight::Medium		=> 1,
			Weight::Heavy		=> 2,
			Weight::SuperHeavy	=> 3,
		}
	}
}

impl fmt::Display for Weight {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{:?}", self)
	}
}

impl PartialOrd for Weight {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		Some(self.num_value().cmp(&other.num_value()))
	}
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum DeviceId {
	Computer(Uuid),
	Passage(Uuid),
	Spaceship(Uuid),
}

impl DeviceId {
	pub fn uuid(self) -> Uuid {
		match self {
			DeviceId::Computer(uuid) => uuid,
			DeviceId::Passage(uuid) => uuid,
			DeviceId::Spaceship(uuid) => uuid,
		}
	}
}

impl fmt::Display for DeviceId {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
		match self {
			DeviceId::Computer(uuid) => write!(
				f, "Computer: {:?}", uuid
			),
			DeviceId::Passage(uuid) => write!(
				f, "Passage: {:?}", uuid
			),
			DeviceId::Spaceship(uuid) => write!(
				f, "Spaceship: {:?}", uuid
			),
		}
	}
}

pub trait Device {
	fn device_id(&self) -> Option<DeviceId>;
	fn set_random_device_id(self) -> Self;
}

// TODO: Since I need Equipments, Food, etc in Creatures (for example), I think it would be better to pass the struct instead of generating it inside the function...?????????????

// pub trait Generator {
// 	type Output;
//
// 	// TODO Uff, buscant per Internet he trobat com fer que compili...
// 	fn generate() -> Generator<Output = Self::Output> where Self: Sized;
// 	fn get(&self, code: &str) -> Self::Output;
// 	fn list(&self) -> Vec<String>;
// }

// TODO fn that takes an Item or Equipment or Food or whatever and inserts it into the HashMap with the name of the thing.
pub trait Generator {
	type Output;

	// TODO When creating the mut HashMap, use with_capacity(CAPACITY_KNOWN_BY_ME)!!!!
	// TODO In every generator I generate the other generators that I'll need there. There may be duplicates (a generates b and c, but b also generates c), so...
	// fn generate() -> Self where Self: Sized;
	/// Returns true if the Generator contains ___ code ___; false otherwise.
	fn contains(&self, code: &str) -> bool;

	/// To check if a given code is valid. Returns Ok if that code __, Err otherwise.
	fn code(&self, code: &str) -> Result<String, String>;
	
	/// Returns the given code if it's valid; panics otherwise.
// 	fn code(&self, code: &'a str) -> &'a str;
	fn get(&self, code: &str) -> Self::Output;
	fn list(&self) -> Vec<&str>;
}

/// A struct that "bundles" all the generators together, so that passing them sep¿p?arately is not needed.
pub struct GeneratorBundle<'a> {
	pub terrain_bundle: TerrainBundle<'a>,

	pub creature_bundle: CreatureBundle<'a>,

	pub item_bundle: ItemBundle<'a>,

	pub recipe_gen: RecipeGen<'a>,

	pub biome_gen: BiomeGen<'a>,
}

impl<'a> Default for GeneratorBundle<'a> {
	fn default() -> GeneratorBundle<'a> {
		let terrain_bundle = TerrainBundle::default();

		let item_bundle = ItemBundle::new();

		let recipe_gen = RecipeGen::generate(&item_bundle, &terrain_bundle);

		let creature_bundle = CreatureBundle::new(&item_bundle);

		let biome_gen = BiomeGen::generate(&terrain_bundle, &creature_bundle, &item_bundle);

		let generator_bundle = GeneratorBundle {
			terrain_bundle,

			creature_bundle,

			item_bundle,

			recipe_gen,

			biome_gen,
		};

		generator_bundle.validate_uniqueness_of_names();

		generator_bundle
	}
}

impl<'a> GeneratorBundle<'a> {
	pub fn gen_terrain(&self, code: &str) -> Terrain {
		self.terrain_bundle.gen_terrain(code)
	}

	pub fn gen_item(&self, code: &str) -> Item {
		self.item_bundle.gen_item(code)
	}

	pub fn gen_equipment(&self, code: &str) -> Equipment {
		self.item_bundle.gen_equipment(code)
	}

	pub fn gen_food(&self, code: &str) -> Food {
		self.item_bundle.gen_food(code)
	}

	pub fn gen_furniture(&self, code: &str) -> Furniture {
		self.item_bundle.gen_furniture(code)
	}

	pub fn gen_plant(&self, code: &str) -> Plant {
		self.item_bundle.gen_plant(code)
	}

	pub fn gen_resource(&self, code: &str) -> Resource {
		self.item_bundle.gen_resource(code)
	}
	
	pub fn gen_tool(&self, code: &str) -> Tool {
		self.item_bundle.gen_tool(code)
	}

	pub fn gen_recipe(&self, code: &str) -> Recipe {
		self.recipe_gen.get(code)
	}

	pub fn gen_creature(&self, code: &str) -> Creature {
		self.creature_bundle.gen_creature(code)
	}

	pub fn gen_humanoid(&self, code: &str) -> Humanoid {
		self.creature_bundle.gen_humanoid(code)
	}

	pub fn gen_animal(&self, code: &str) -> Animal {
		self.creature_bundle.gen_animal(code)
	}

	pub fn gen_spaceship(&self, code: &str) -> Spaceship {
		self.creature_bundle.gen_spaceship(code)
	}

	pub fn gen_biome(&self, code: &str) -> Biome {
		self.biome_gen.get(code)
	}

	/// Checks/Verifies/Validates that all names/codes are unique ¿between? all the Generators.
	fn validate_uniqueness_of_names(&self) {//-> Result<(), String> {
		// TODO Move to a separate method? I think it's not needed, because I only need this here... Does it smell (omg how pro)?
		let bundles: &[Vec<&str>] = &[
			self.terrain_bundle.foundation_gen.list(),
			self.terrain_bundle.liquid_gen.list(),

			self.creature_bundle.animal_gen.list(),
			self.creature_bundle.humanoid_gen.list(),
			self.creature_bundle.spaceship_gen.list(),

			self.item_bundle.equipment_gen.list(),
			self.item_bundle.food_gen.list(),
			self.item_bundle.furniture_gen.list(),
			self.item_bundle.plant_gen.list(),
			// self.item_bundle.recipe_gen.list(),
			self.item_bundle.resource_gen.list(),
			self.item_bundle.tool_gen.list(),

			self.biome_gen.list()
		];

		let capacity = || bundles.iter().map(|x| x.len()).into_iter().sum();

		let mut unique_names: HashSet<&str> = HashSet::with_capacity(capacity());

		for bundle in bundles {
			for name in bundle {
				if !unique_names.insert(name) {
					panic!("Non unique name: {}", name);
				}
			}
		}
	}
}

// TODO Add a method to list them as String or &str? &'static str? Maybe with a slice ([Self]) instead of a Vec... In engine::actions::recipes I list() Profession and transform that Vec into a Vec<String> and then into a Vec<&str>... Maybe I should put that in that Trait.
pub trait ListEnum {
	fn list() -> Vec<Self> where Self: Sized; 
}

// TODO Huh, to change the "icon pack" I only have to use another font... And fonts don't have that many characters, so I can't even put two full icon packs in just one...
// pub enum Icon {
// 	Player,
// 	Orc,
// 	Troll,
// 	Elf,
// }
//
// pub trait IconPack<T> {
// 	fn get_icon(&self, icon: Icon) -> ();
// }

pub enum Temperature {
	Freezing,
	Cold,
	Cool,
	NORMAL,
	Warm,
	Hot,
	Scorching,
}