use std::collections::HashMap;

use crate::{Item, ToItem, Device, DeviceId, Color, Size, Weight, Generator};

pub mod bed;
use bed::{Bed, insert_beds};

pub mod computer;
pub use computer::{Computer, insert_computers};

// pub mod energy_source;

pub mod generic;
pub use generic::{Generic, insert_generics};

pub mod passage;
use passage::{Passage, TeleportLocation, insert_passages};

pub mod storage;
use storage::{Storage, insert_storages};

pub mod workshop;
pub use workshop::{Workshop, insert_workshops};

pub mod lock;
pub use lock::Lock;

pub trait Toggle {
	fn toggle(&mut self);
	// fn can_be_linked(&self) -> bool;
	fn can_be_linked(&self) -> Result<(), String>;
	fn link(&mut self);
}


pub trait ToFurniture {
	fn to_furniture(self) -> Furniture;
	// 	fn rotate(&mut self) {}
	// 	fn use(&mut self) {}
	// 	fn carry(&mut self, holder: &Creature) {}
}

#[derive(Clone, Serialize, Deserialize)]
pub enum Furniture {
	Bed(Bed),
	Storage(Storage),
	Computer(Computer),
	Passage(Passage),
// 	Light(Light<'a>),
//	PowerSource(PowerSource<'a>),
	Generic(Generic),
	Workshop(Workshop),
}

impl ToFurniture for Furniture {
	fn to_furniture(self) -> Furniture {
		self
	}
}

impl Device for Furniture {
	fn device_id(&self) -> Option<DeviceId> {
		match self {
			Furniture::Bed(..) => None,
			Furniture::Storage(..) => None,
			Furniture::Computer(computer) => computer.device_id(),
			Furniture::Passage(passage) => passage.device_id(),
			Furniture::Generic(..) => None,
			Furniture::Workshop(..) => None,
		}
	}

	fn set_random_device_id(self) -> Furniture {
		match self {
			Furniture::Computer(computer) => {
				computer.set_random_device_id().to_furniture()
			},
			Furniture::Passage(passage) => {
				passage.set_random_device_id().to_furniture()
			},
			other => other,
		}
	}
}

impl Toggle for Furniture {
	fn toggle(&mut self) {
		// panic!("You shouldn't call toogle on Furniture... -_-");
		match self {
			Furniture::Passage(passage)	=> passage.toggle(),
			// Furniture::Computer(computer) => computer.toggle(),
			// Furniture::Storage(storage) => storage.toggle(),
			_ => panic!("called toggle on un-toggable furniture"),
		}
	}

	fn can_be_linked(&self) -> Result<(), String> {
		match self {
			Furniture::Passage(passage)		=> passage.can_be_linked(),
			// Furniture::Computer(computer)	=> computer.can_be_linked(),
			// Furniture::Storage(storage)		=> storage.can_be_linked(),
			_								=> Err(String::from("CAN'T EVER BE LINKED F")),
		}
	}

	fn link(&mut self) {
		// panic!("and i say nooo noooooooo noo");
		match self {
			Furniture::Passage(passage)		=> passage.link(),
			// Furniture::Computer(computer)	=> computer.link(),
			// Furniture::Storage(storage)		=> storage.link(),
			_								=> panic!("and i say nooo no noo"),
		};
	}
}

impl ToItem for Furniture {
	fn to_item(self) -> Item {
		Item::Furniture(Box::new(self))
	}

	fn name(&self) -> &str {
		match self {
			Furniture::Bed(bed)				=> &bed.name,
			Furniture::Storage(storage)		=> &storage.name,
			Furniture::Computer(computer)	=> &computer.name,
			Furniture::Passage(passage)		=> &passage.name,
// 			Furniture::Light(light)			=> light.name,
			Furniture::Generic(generic)		=> &generic.name,
			Furniture::Workshop(workshop)	=> &workshop.name,
		}
	}

	fn icon(&self) -> char {
		match self {
			Furniture::Bed(bed)				=> bed.icon,
			Furniture::Storage(storage)		=> storage.icon,
			Furniture::Computer(computer)	=> computer.icon,
			Furniture::Passage(passage)		=> passage.icon(),
// 			Furniture::Light(light)			=> light.icon,
			Furniture::Generic(generic)		=> generic.icon,
			Furniture::Workshop(workshop)	=> workshop.icon,
		}
	}

	fn color(&self) -> Color {
		match self {
			Furniture::Bed(bed)				=> bed.color,
			Furniture::Storage(storage)		=> storage.color,
			Furniture::Computer(computer)	=> computer.color,
			Furniture::Passage(passage)		=> passage.color(),
			Furniture::Generic(generic)		=> generic.color,
			Furniture::Workshop(workshop)	=> workshop.color,
		}
	}

	fn blocks_passage(&self) -> bool {
		match self {
			Furniture::Bed(bed)				=> bed.blocks_passage,
			Furniture::Storage(storage)		=> storage.blocks_passage,
			Furniture::Computer(computer)	=> computer.blocks_passage,
			Furniture::Passage(passage)		=> passage.blocks_passage,
			Furniture::Generic(generic)		=> generic.blocks_passage,
			Furniture::Workshop(_workshop)	=> true,
		}
	}

	fn size(&self) -> Size {
		match self {
			Furniture::Bed(bed) => bed.size(),
			Furniture::Storage(storage) => storage.size(),
			Furniture::Computer(computer) => computer.size(),
			Furniture::Passage(passage) => passage.size(),
			Furniture::Generic(generic) => generic.size(),
			Furniture::Workshop(workshop) => workshop.size(),
		}
	}

	fn weight(&self) -> Weight {
		match self {
			Furniture::Bed(bed) => bed.weight(),
			Furniture::Storage(storage) => storage.weight(),
			Furniture::Computer(computer) => computer.weight(),
			Furniture::Passage(passage) => passage.weight(),
			Furniture::Generic(generic) => generic.weight(),
			Furniture::Workshop(workshop) => workshop.weight(),
		}
	}

	fn price(&self) -> u16 {
		match self {
			Furniture::Bed(bed)				=> bed.price,
			Furniture::Storage(storage)		=> storage.price,
			Furniture::Computer(computer)	=> computer.price,
			Furniture::Passage(passage)		=> passage.price,
			Furniture::Generic(generic)		=> generic.price,
			Furniture::Workshop(workshop)	=> workshop.price,
		}
	}
}

pub struct FurnitureGen<'a> {
	hashmap: HashMap<&'a str, Furniture>,
}

impl<'a> FurnitureGen<'a> {
	pub fn generate() -> FurnitureGen<'a> {
		let mut hashmap: HashMap<&str, Furniture> = HashMap::new();

		insert_generics(&mut hashmap);
		insert_passages(&mut hashmap);
		insert_storages(&mut hashmap);
		insert_computers(&mut hashmap);
		insert_workshops(&mut hashmap);
		insert_beds(&mut hashmap);

		FurnitureGen {
			hashmap,
		}
	}
}

impl<'a> Generator for FurnitureGen<'a> {
	type Output = Furniture;

	fn contains(&self, code: &str) -> bool {
		self.hashmap.contains_key(code)
	}

	fn code(&self, code: &str) -> Result<String, String> {
		if self.hashmap.contains_key(code) {
			Ok(code.to_string())
		}
		else {
			Err(format!("Code {} not valid in FurnitureGen", code))
		}
	}

	fn get(&self, code: &str) -> Furniture {
		self.hashmap.get(code)
			.unwrap_or_else(|| panic!("Code {} not valid in FurnitureGen", code))
			.clone()
			.set_random_device_id()
	}

	fn list(&self) -> Vec<&str> {
		let mut vec: Vec<&str> = Vec::with_capacity(self.hashmap.len());
		for key in self.hashmap.keys() {
			vec.push(key);
		}
		vec.sort();
		vec
	}
}

impl<'a> FurnitureGen<'a> {
	/// Generate a Passage with a random Uuid and an ```Option```al ```TeleportLocation```.
	pub fn gen_passage(
		&self,
		code: &str,
		teleport_location: Option<TeleportLocation>
	) -> Passage {
		if let Some(Furniture::Passage(passage)) = self.hashmap.get(code) {
			let mut passage_final = passage.clone();
			passage_final.teleport_location = teleport_location;
			passage_final.set_random_device_id()
		} else {
			panic!()
		}
	}

	pub fn gen_computer(&self, code: &str) -> Computer {
		if let Some(Furniture::Computer(computer)) = self.hashmap.get(code) {
			computer.clone().set_random_device_id()
		} else {
			panic!()
		}
	}
}