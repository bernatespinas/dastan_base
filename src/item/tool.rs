use std::collections::HashMap;

use crate::{
	Color, Size, Weight, Generator,
	Food,
	Item, ToItem,
};

// pub trait ToTool<'a> {
// 	fn to_tool(self) -> Tool<'a>;
// }

// struct BasicInfo<'a> {
// 	name: &'a str,
// 	icon: char,
// 	color: Color,
// }

// struct ItemInfo<'a> {
// 	basic_info: BasicInfo<'a>,
// 	price: u16,
// }

// Huh... Should I create a struct for every tool...? Then, I'd have to create a Tool enum... Hmm, the only difference between each type of tool is/ Well, some tools like axes and pickaxes (and shovels (?)...) are used to "destroy" things, so the only difference between them is what type of thing they "destroy". But then there are fishing ¿rods?, water ¿can? / watering ????...

#[derive(Clone, Serialize, Deserialize)]
pub enum Tool {
	Pickaxe(Pickaxe),
	Axe(Axe),
	FishingRod(FishingRod),
}

impl ToItem for Tool {
	fn to_item(self) -> Item {
		Item::Tool(Box::new(self))
	}

	fn name(&self) -> &str {
		match self {
			Tool::Pickaxe(pickaxe)			=> &pickaxe.name,
			Tool::Axe(axe)					=> &axe.name,
			Tool::FishingRod(fishing_rod)	=> &fishing_rod.name,
		}
	}

	fn icon(&self) -> char {
		match self {
			Tool::Pickaxe(pickaxe)			=> pickaxe.icon,
			Tool::Axe(axe)					=> axe.icon,
			Tool::FishingRod(fishing_rod)	=> fishing_rod.icon,
		}
	}

	fn color(&self) -> Color {
		match self {
			Tool::Pickaxe(pickaxe)			=> pickaxe.color,
			Tool::Axe(axe)					=> axe.color,
			Tool::FishingRod(fishing_rod)	=> fishing_rod.color,
		}
	}

	fn blocks_passage(&self) -> bool {
		false
	}

	fn size(&self) -> Size {
		Size::Small
	}

	fn weight(&self) -> Weight {
		Weight::Light
	}

	fn price(&self) -> u16 {
		match self {
			Tool::Pickaxe(pickaxe)			=> pickaxe.price,
			Tool::Axe(axe)					=> axe.price,
			Tool::FishingRod(fishing_rod)	=> fishing_rod.price,
		}
	}
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Pickaxe {
	name: String,
	icon: char,
	color: Color,

	price: u16,

	power: u8,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Toolbox {
	name: String,
	icon: char,
	color: Color,

	price: u16,

	complexity: u8,
}

// impl<'a> ToTool<'a> for Pickaxe<'a> {
// 	fn to_tool(self) -> Tool<'a> {
// 		Tool::Pickaxe(self)
// 	}
// }

impl From<Pickaxe> for Tool {
	fn from(pickaxe: Pickaxe) -> Tool {
		Tool::Pickaxe(pickaxe)
	}
}

// impl<'a> From<Pickaxe<'a>> for Equipment<'a> {
// 	fn from(pickaxe: Pickaxe<'a>) -> Equipment<'a> {
// 		Equipment::Weapon
// 	}
// }

#[derive(Clone, Serialize, Deserialize)]
pub struct Axe {
	name: String,
	icon: char,
	color: Color,

	price: u16,

	power: u8,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct FishingRod {
	name: String,
	icon: char,
	color: Color,

	price: u16,

	power: u8,

	bait: Option<Food>,
}

pub struct ToolGen<'a> {
	hashmap: HashMap<&'a str, Tool>,
}

impl<'a> ToolGen<'a> {
	pub fn generate() -> ToolGen<'a> {
		let mut hashmap: HashMap<&str, Tool> = HashMap::new();

		hashmap.insert("Le pickaxe", Pickaxe {
			name: "Le pickaxe".to_string(),
			icon: '%',
			color: Color::LGrey,

			price: 666,

			power: 255,
		}.into());

		ToolGen {
			hashmap,
		}
	}
}

impl<'a> Generator for ToolGen<'a> {
	type Output = Tool;

	fn contains(&self, code: &str) -> bool {
		self.hashmap.contains_key(code)
	}

	fn code(&self, code: &str) -> Result<String, String> {
		if self.hashmap.contains_key(code) {
			Ok(code.to_string())
		}
		else {
			Err(format!("Code {} not valid in ToolGen", code))
		}
	}

	fn get(&self, code: &str) -> Tool {
		self.hashmap.get(code)
			.unwrap_or_else(|| panic!("Code {} not valid in ToolGen", code))
			.clone()
	}

	fn list(&self) -> Vec<&str> {
		let mut vec: Vec<&str> = Vec::with_capacity(self.hashmap.len());
		for key in self.hashmap.keys() {
			vec.push(key);
		}
		vec.sort();
		vec
	}
}