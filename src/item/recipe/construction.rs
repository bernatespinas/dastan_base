use std::collections::HashMap;
use super::{Recipe, BLUEPRINT, Profession::{
	// Carpentry,
	// Masonry,
	// Welding,

	Walling,
	Flooring,
}};
use crate::{Color, Generator, ItemBundle, TerrainBundle};

pub fn insert_construction_recipes(
	hashmap: &mut HashMap<&str, Recipe>,
	item_bundle: &ItemBundle,
	terrain_bundle: &TerrainBundle
) {
	hashmap.insert("Wood wall blueprint", Recipe {
		name: "Wood wall blueprint".to_string(),
		icon: BLUEPRINT,
		color: Color::Yellow,

		price: 80,

		input: vec!(
			item_bundle.resource_gen.code("Wood").unwrap(),
		),

		output: terrain_bundle.foundation_gen.code("WALL_WOOD").unwrap(),

		profession: Walling,
	});

	hashmap.insert("Wood floor blueprint", Recipe {
		name: "Wood floor blueprint".to_string(),
		icon: BLUEPRINT,
		color: Color::Yellow,

		price: 80,

		input: vec!(
			item_bundle.resource_gen.code("Wood").unwrap(),
		),

		output: terrain_bundle.foundation_gen.code("FLOOR_WOOD").unwrap(),

		profession: Flooring,
	});

	hashmap.insert("Stone wall blueprint", Recipe {
		name: "Stone wall blueprint".to_string(),
		icon: BLUEPRINT,
		color: Color::DGrey,

		price: 80,

		input: vec!(
			item_bundle.resource_gen.code("Stone").unwrap(),
		),
		output: //vec!(
			terrain_bundle.foundation_gen.code("WALL_STONE").unwrap(),
		// ),

		profession: Walling,
	});

	hashmap.insert("Stone floor blueprint", Recipe {
		name: "Stone floor blueprint".to_string(),
		icon: BLUEPRINT,
		color: Color::Yellow,

		price: 80,

		input: vec!(
			item_bundle.resource_gen.code("Stone").unwrap(),
		),

		output: terrain_bundle.foundation_gen.code("FLOOR_STONE").unwrap(),

		profession: Flooring,
	});
}