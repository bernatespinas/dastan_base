pub struct Key {
	id: String,
}

pub struct Keycard {
	id: String,
}

impl Keycard {
	pub fn new(id: String) -> Keycard {
		Keycard {
			id,
		}
	}
}