pub struct Book {
	title: String,
	// icon: char,
	color: Color,

	price: u16,

	recipe_book: Option<RecipeBook>,
	recipes: Option<Vec<String>>,
	// spell_book: Option<SpellBook>,
	// locations: Option<LocationGuide>,
}

impl ToItem for Book {
	fn to_item(self) -> Item {
		Item::Book(Box::new(self))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		BOOK
	}

	fn color(&self) -> Color {
		self.color
	}

	fn blocks_passage(&self) -> bool {
		false
	}

	fn price(&self) -> u16 {
		self.price
	}
}