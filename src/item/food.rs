use std::collections::HashMap;

use crate::{Item, ToItem, Color, Size, Weight, Generator};

// const MEAT: char = 'í';
const POTION: char = 'à';
// const APPLE: char = 'ä';
// const Orange_FRUIT: char = 'ä';
// const LEMON: char = 'ä';

#[derive(Debug, PartialEq, Copy, Clone, Serialize, Deserialize)]
pub enum Effect {
	Feed(u16),
	Heal(u16),
	// Poison(u32),
	// Kill,
	None,
}

impl std::fmt::Display for Effect {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
		write!(f, "{:?}", self)
	}
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Food {
	pub name: String,
	pub icon: char,
	pub color: Color,

	pub price: u16,

	pub effect: Effect,
}

impl ToItem for Food {
	fn to_item(self) -> Item {
		Item::Food(Box::new(self))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn color(&self) -> Color {
		self.color
	}

	fn size(&self) -> Size {
		Size::Small
	}

	fn weight(&self) -> Weight {
		Weight::Light
	}

	fn blocks_passage(&self) -> bool {
		false
	}

	fn price(&self) -> u16 {
		self.price
	}
}

pub struct FoodGen<'a> {
	hashmap: HashMap<&'a str, Food>,
}

impl<'a> FoodGen<'a> {
	pub fn generate() -> FoodGen<'a> {
		let mut hashmap: HashMap<&str, Food> = HashMap::new();

		hashmap.insert("POTION_HEALING", Food {
			name: "Healing potion".to_string(),
			icon: POTION,
			color: Color::DPink,

			price: 20,

			effect: Effect::Heal(20),
		});

		hashmap.insert("POTION_FEEDING", Food {
			name: "Feeding potion".to_string(),
			icon: POTION,
			color: Color::LPurple,

			price: 15,

			effect: Effect::Feed(30),
		});

		// hashmap.insert("APPLE_RED", Food {
		// 	name: "Red apple".to_string(),
		// 	icon:
		// })

		FoodGen {
			hashmap
		}
	}
}

impl<'a> Generator for FoodGen<'a> {
	type Output = Food;

	fn contains(&self, code: &str) -> bool {
		self.hashmap.contains_key(code)
	}

	fn code(&self, code: &str) -> Result<String, String> {
		if self.hashmap.contains_key(code) {
			Ok(code.to_string())
		}
		else {
			Err(format!("Code {} not valid in FoodGen", code))
		}
	}

	fn get(&self, code: &str) -> Food {
		self.hashmap.get(code)
			.unwrap_or_else(|| panic!("Code {} not valid in FoodGen", code))
			.clone()
	}

	fn list(&self) -> Vec<&str> {
		let mut vec: Vec<&str> = Vec::with_capacity(self.hashmap.len());
		for key in self.hashmap.keys() {
			vec.push(key);
		}
		vec.sort();
		vec
	}
}