use std::collections::HashMap;

use crate::{Color, ListEnum};
use crate::{
	// Item, ToItem,
	Generator, ItemBundle, TerrainBundle
};

mod construction;

// const RECIPE: char = 'ý';
const BLUEPRINT: char = 'y';

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone, Serialize, Deserialize)]
pub enum Profession {
	Butcher,
	Fisher,
	Herbalist,
	Farmer,
	Miner,

	Cook,
	Alchemist,
	FirstAid,

	Taylor,
	Leatherworking,
	Metalsmith,

	// Construction,
	Carpentry,
	Masonry,
	Welding,

	Flooring,
	Walling,
}

impl std::fmt::Display for Profession {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
		write!(f, "{:?}", self)
	}
}

impl ListEnum for Profession {
	fn list() -> Vec<Profession> {
		vec!(
			Profession::Butcher,
			Profession::Fisher,
			Profession::Herbalist,
			Profession::Farmer,
			Profession::Miner,
			Profession::Cook,
			Profession::Alchemist,
			Profession::FirstAid,
			Profession::Taylor,
			Profession::Leatherworking,
			Profession::Metalsmith,
			// Profession::Construction,
			Profession::Carpentry,
			Profession::Masonry,
			Profession::Welding,
			Profession::Flooring,
			Profession::Walling)
	}
}

impl Profession {
	/// Lists crafting professions, that is, ___.
	pub fn list_crafting() -> Vec<Profession> {
		vec!(
			Profession::Butcher,
			Profession::Fisher,
			Profession::Herbalist,
			Profession::Farmer,
			Profession::Miner,
			Profession::Cook,
			Profession::Alchemist,
			Profession::FirstAid,
			Profession::Taylor,
			Profession::Leatherworking,
			Profession::Metalsmith
		)
	}

	pub fn list_construction() -> Vec<Profession> {
		vec!(
			Profession::Carpentry,
			Profession::Masonry,
			Profession::Welding,
			Profession::Flooring,
			Profession::Walling
		)
	}
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Recipe {
	pub name: String,
	pub icon: char,
	pub color: Color,

	pub price: u16,

	/// The names of the ingredients.
	pub input: Vec<String>,
	// TODO Should recipes produce multiple different items (maybe), or just one item? Maybe one or more of the same item? I could store an amount attribute, but...
	// If there isn't enough space on the player's inventory, what should I do? Should crafting fail? I might not be able to put items on the ground since there might already be an item there...
	// A Vec containing the *codes* of the ¿final product?.
	// pub output: Vec<String>,
	/// The name of the output.
	pub output: String,

	pub profession: Profession,
	// workshop: Option<Workshop>,
}

impl Recipe {
	/// Returns the name of the Recipe without " recipe" in it.
	pub fn craft_name(&self) -> &str {
		// // self.name.split_at(self.name.len() - " recipe".len()).0
		// self.name.split(' ')
		// 	.map()

		// self.name.trim_end_matches()
		&self.output
	}
}

// TODO HashSet<Recipe<'a>> instead of a Vec?
	// + No duplicates, fast access.
	// - I have to see if I'd need to collect() it into a Vec for Scrollable or other things to work. If I have to, maybe it's more worth ¿it? / worthier to add a HashSet<&'a str> containing the names of every added recipe, to easily prevent duplicates. 
#[derive(Clone, Serialize, Deserialize)]
pub struct RecipeBook {
	recipes: HashMap<Profession, Vec<Recipe>>,
}

impl Default for RecipeBook {
	fn default() -> RecipeBook {
		let mut recipes: HashMap<Profession, Vec<Recipe>> = HashMap::new();

		for profession in Profession::list() {
			recipes.insert(profession, Vec::new());
		}

		RecipeBook {
			recipes,
		}
	}
}

impl RecipeBook {
	pub fn with_recipes(mut self, recipes: Vec<Recipe>) -> RecipeBook {
		for recipe in recipes {
			self.recipes.get_mut(&recipe.profession).unwrap().push(recipe);
		}

		self
	}

	pub fn by_profession(&self, profession: &Profession) -> &Vec<Recipe> {
		self.recipes.get(profession).unwrap()
	}

	pub fn remove(&mut self, profession: Profession, i: usize) -> Recipe {
		self.recipes.get_mut(&profession).unwrap().remove(i)
	}

	pub fn already_known(&self, recipe: &Recipe) -> bool {
		for learnt_recipes in self.recipes.get(&recipe.profession) {
			for name in learnt_recipes.iter().map(|r| &r.name) {
				if name == &recipe.name {
					return true;
				}
			}
		}

		false
	}

	pub fn add_recipe(&mut self, recipe: Recipe) -> Result<(), String> {
		if self.already_known(&recipe) {
			Err("This recipe is already known!".to_string())
		}
		else {
			self.recipes.get_mut(&recipe.profession).unwrap().push(recipe);
			Ok(())
		}
	}
}

pub struct RecipeGen<'a> {
	hashmap: HashMap<&'a str, Recipe>,
}

impl<'a> RecipeGen<'a> {
	pub fn generate(
		item_bundle: &ItemBundle,
		terrain_bundle: &TerrainBundle
	) -> RecipeGen<'a> {

		let mut hashmap: HashMap<&str, Recipe> = HashMap::with_capacity(40);

		// butcher::insert_butcher_recipes(&mut hashmap);
		// fisher::insert_fisher_recipes(&mut hashmap);
		// herbalist::insert_herbalist_recipes(&mut hashmap);
		// farmer::insert_farmer_recipes(&mut hashmap);
		// miner::insert_miner_recipes(&mut hashmap);
		// cook::insert_cook_recipes(&mut hashmap);
		// alchemist::insert_alchemist_recipes(&mut hashmap);
		// taylor::insert_taylor_recipes(&mut hashmap);
		// first_aid::insert_first_aid_recipes(&mut hashmap);
		construction::insert_construction_recipes(&mut hashmap, item_bundle, terrain_bundle);
		// weapon_smith::insert_weapon_smith_recipes(&mut hashmap);
		// armouru

		hashmap.insert("Pickaxe", Recipe {
			name: "Pickaxe".to_string(),
			icon: 'P',
			color: Color::LGrey,

			price: 45,

			input: vec!(
				item_bundle.resource_gen.code("Stone").unwrap(),
				item_bundle.resource_gen.code("Wood").unwrap()
			),
			output:// vec!(
				item_bundle.tool_gen.code("Le pickaxe").unwrap(),
			// ),

			profession: Profession::Miner,
		});

		RecipeGen {
			hashmap,
		}
	}
}

impl<'a> Generator for RecipeGen<'a> {
	type Output = Recipe;

	fn contains(&self, code: &str) -> bool {
		self.hashmap.contains_key(code)
	}

	fn code(&self, code: &str) -> Result<String, String> {
		if self.hashmap.contains_key(code) {
			Ok(code.to_string())
		}
		else {
			Err(format!("Code {} not valid in RecipeGen", code))
		}
	}

	fn get(&self, code: &str) -> Recipe {
		self.hashmap.get(code)
			.unwrap_or_else(|| panic!("Code {} not valid in RecipeGen", code))
			.clone()
	}

	fn list(&self) -> Vec<&str> {
		let mut vec: Vec<&str> = Vec::with_capacity(self.hashmap.len());
		for key in self.hashmap.keys() {
			vec.push(key);
		}
		vec.sort();
		vec
	}
}