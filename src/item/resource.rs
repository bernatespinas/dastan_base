use std::collections::HashMap;

use crate::{Item, ToItem, Color, Size, Weight, Generator};

// Resources.
const BONE: char = 'ñ';
// const LEATHER: char = 'â';
// const FEATHER: char = 'h';

// pub enum Resource {
// 	Wood(Wood),
// 	Stone(Stone),
// 	Steel(Steel),
// 	Leather(Leather),
// 	Bone(Bone),
// 	Feather(Feather),
// }

#[derive(Clone, Serialize, Deserialize)]
pub struct Resource {
	pub name: String,
	pub icon: char,
	pub color: Color,

	pub price: u16,
}

impl ToItem for Resource {
	fn to_item(self) -> Item {
		Item::Resource(Box::new(self))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn color(&self) -> Color {
		self.color
	}

	fn blocks_passage(&self) -> bool {
		false
	}

	fn size(&self) -> Size {
		Size::Small
	}

	fn weight(&self) -> Weight {
		Weight::Light
	}

	fn price(&self) -> u16 {
		self.price
	}
}

pub struct ResourceGen<'a> {
	hashmap: HashMap<&'a str, Resource>,
}

impl<'a> ResourceGen<'a> {
	pub fn generate() -> ResourceGen<'a> {
		let mut hashmap: HashMap<&str, Resource> = HashMap::new();
		
		hashmap.insert("Stone", Resource {
			name: "Stone".to_string(),
			icon: '.',
			color: Color::DGrey,

			price: 15,
		});
		
		hashmap.insert("Wood", Resource {
			name: "Wood".to_string(),
			icon: '_',
			color: Color::LBrown,

			price: 10,
		});

		hashmap.insert("Steel", Resource {
			name: "Steel".to_string(),
			icon: '+',
			color: Color::LGrey,

			price: 30,
		});

		hashmap.insert("Leather", Resource {
			name: "Leather".to_string(),
			icon: '¬',
			color: Color::DBrown,

			price: 20,
		});

		hashmap.insert("Bone", Resource {
			name: "Bone".to_string(),
			icon: BONE,
			color: Color::LGrey,

			price: 15,
		});
		
		hashmap.insert("Feather", Resource {
			name: "Feather".to_string(),
			icon: '/',
			color: Color::LGrey,

			price: 5,
		});

		hashmap.insert("Cloth", Resource {
			name: "Cloth".to_string(),
			icon: '#',
			color: Color::LGrey,

			price: 25,
		});

		hashmap.insert("String", Resource {
			name: "String".to_string(),
			icon: '~',
			color: Color::LGrey,

			price: 10,
		});

		hashmap.insert("Glass", Resource {
			name: "Glass".to_string(),
			icon: '#',
			color: Color::LBlue,

			price: 45,
		});

		hashmap.insert("Copper", Resource {
			name: "Copper".to_string(),
			icon: '.',
			color: Color::LOrange,

			price: 25,
		});

		hashmap.insert("Silver", Resource {
			name: "Silver".to_string(),
			icon: '.',
			color: Color::LGrey,

			price: 75,
		});

		hashmap.insert("Gold", Resource {
			name: "Gold".to_string(),
			icon: '.',
			color: Color::LYellow,

			price: 115,
		});

		hashmap.insert("Iron", Resource {
			name: "Iron".to_string(),
			icon: '.',
			color: Color::DGrey,

			price: 55,
		});

		hashmap.insert("Metal/Iron/Steel parts", Resource {
			name: "Metal/Iron/Steel parts".to_string(),
			icon: '%',
			color: Color::LGrey,

			price: 85,
		});
		
		ResourceGen {
			hashmap,
		}
	}
}

impl<'a> Generator for ResourceGen<'a> {
	type Output = Resource;

	fn contains(&self, code: &str) -> bool {
		self.hashmap.contains_key(code)
	}

	fn code(&self, code: &str) -> Result<String, String> {
		if self.hashmap.contains_key(code) {
			Ok(code.to_string())
		}
		else {
			Err(format!("Code {} not valid in ResourceGen", code))
		}
	}

	fn get(&self, code: &str) -> Resource {
		self.hashmap.get(code)
			.unwrap_or_else(|| panic!("Code {} not valid in ResourceGen", code))
			.clone()
	}

	fn list(&self) -> Vec<&str> {
		let mut vec: Vec<&str> = Vec::with_capacity(self.hashmap.len());
		for key in self.hashmap.keys() {
			vec.push(key);
		}
		vec.sort();
		vec
	}
}