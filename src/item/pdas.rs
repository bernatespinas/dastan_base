pub struct Pda {
	username: String,
	password: String,

	battery: u16,
	powered_on: bool,

	/// To keep track of the amount of time the PDA has been plugged int, .to. calculate how much battery it has gained.
	plugged_in_start: Option<usize>,
}

impl Pda {
	pub fn is_powered_on(&self) -> bool {
		self.powered_on
	}

	pub fn power_on(&mut self) {
		self.decrease_battery_by(10);
		self.powered_on = true;
	}

	pub fn power_off(&mut self) {
		self.decrease_battery_by(5);
		self.powered_on = false;
	}

	pub fn plug_in(&mut self) {
		self.plugged_in_start = Some(CURRENT_TIME);
	}

	pub fn plug_huh_out_huh(&mut self) {
		let plugged_in_start = self.plugged_in_start.unwrap();
		let time_delta = CURRENT_TIME - plugged_in_start;
		let battery += time_delta/??;
		self.plugged_in_start = None;
		// TODO replace option
	}

	pub fn decrease_battery_by(&mut self, amount: u16) {
		if amount >= self.battery {
			self.battery = 0;
		}
		else {
			self.battery -= amount;
		}
	}

	pub fn retrieve_mail(&self, internet: &Internet) {

	}

	pub fn retrieve_files(&self, internet: &Internet) {

	}
}