pub struct BasicInfo<'a> {
	name: &'a str,
	icon: char,
	color: Color,
	blocks_passage: bool,
	blocks_sight: bool,
	// category: Category,
}

pub struct Battery<'a> {
	basic_info: BasicInfo<'a>,

	charge_level: u8,
}

pub struct BatteryCharger<'a> {
	basic_info: BasicInfo<'a>,
	battery: Option<Battery<'a>>,
}

impl<'a> BatteryCharger<'a> {
	pub fn battery_in(&mut self, battery: Battery<'a>) {
		self.battery = Some(battery);
	}

	pub fn battery_in2(&mut self, battery: Battery<'a>) -> Result<(), Battery<'a>> {
		if self.battery.is_none() {
			self.battery = Some(battery);
			return Ok(());
		}
		else {
			return Err(battery);
		}
	}

	pub fn battery_out_OR_take_battery_OR_battery_take(&mut self) -> {
		// UPDATE Wtf?? -_-
	}
}

// struct AIs {
// 
// }

pub trait TakeTurn {
	fn take_turn(&mut self);
}

// TODO Hmm, I don't want every BatteryCharger to constantly check if it has a battery inside... I only want them to do something when there actually is one there. Maybe I should make them "register" as a TakeTurn when a battery is available? And I could use an unwrap() in fn take_turn because it should only be called when ___.
// UPDATE I guess I would register them by giving them an AI and storing their position in the item_ais Vec or something. Also, I think it would be better if, instead of increasing the battery level constantly, I calculate the time that has passed since the battery has been plugged in and use it to increase its level. I could set a stopped_at time variable when the power gets cut off, and remove if it comes back. So, it could work.
impl TakeTurn for BatteryCharger {
	fn take_turn(&mut self) {
		if self.battery = Some(battery)
	}
}

pub struct SolarPanel {

}

pub struct Wind¿Mill? {

}

pub struct Water¿Mill? {

}

pub struct NeedsWoodGenerator {

}

pub struct PowerGenerator {
	sunlight: Option<u8>,
	wind: Option<u8>,
	organic_matter: Option<u8>,

	output: u16,
}