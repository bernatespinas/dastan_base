use std::collections::HashMap;

use crate::{Item, ToItem, Color, Size, Weight, Generator};

#[derive(Clone, Serialize, Deserialize)]
pub struct Plant {
	pub name: String,
	pub icon: char,
	pub color: Color,

	pub blocks_passage: bool,
	blocks_sight: bool,
	size: Size,
	weight: Weight,

	pub price: u16,

	// Hmmm... Food? Resource? Una planta fa pomes, una altra cotó...
	// Huh, enum PlantThingy { Food(Food), Resource(Resource) };????
	// I also could make a fruit (Food) that can be eaten (or not), and that can be processed into cotton or other Resources...
	// drops: Vec<Resource>,

	// Bool or amount?
	// is_alive: bool,

	// The amount of water the plant currently has. (?) ¿By? how much will it decrease every turn? Should it happen every turn? -1 all the time? When producing fruits or being harvested? Hmm, that way I wouldn't be checking and modifying all the time!
	// Should I put a different value for each plant? Or could I make all of them have 1000 or 10000 or something, and in harvest() or whatever decrease it by a specific amount?
	// water_level: u16,

	// TODO Hmm, should all plants need sunlight? Maybe I could create Fungus or something, which wouldn't need sunlight or only grow in darkness, not water (...?).
	// needs_sunlight: bool,

	// grows: Option<???>,
	pub harvest: Vec<String>,
	// every: amount_of_time,
}

// impl<'a> Plant<'a> {
	// fn water(&mut self) {
	//
	// }
	//
	// fn harvest(&mut self) -> Vec<????> {
	//
	// }
	//
	// fn dry(&mut self) {
	// 	self.is_alive = false;
	// 	self.color = brown or grey;
	// 	self.drops = vec!(dry_thingies);
	// }
// }

impl ToItem for Plant {
	fn to_item(self) -> Item {
		Item::Plant(Box::new(self))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn color(&self) -> Color {
		self.color
	}

	fn blocks_passage(&self) -> bool {
		self.blocks_passage
	}

	fn size(&self) -> Size {
		self.size
	}

	fn weight(&self) -> Weight {
		self.weight
	}

	fn price(&self) -> u16 {
		self.price
	}
}

pub struct PlantGen<'a> {
	hashmap: HashMap<&'a str, Plant>,
}

impl<'a> PlantGen<'a> {
	pub fn generate() -> PlantGen<'a> {
		let mut hashmap: HashMap<&str, Plant> = HashMap::new();

		hashmap.insert("Pine", Plant {
			name: "Pine".to_string(),
			icon: 'p',
			color: Color::DGreen,

			blocks_passage: true,
			blocks_sight: true,
			size: Size::Huge,
			weight: Weight::Heavy,

			price: 999,

			harvest: Vec::new(),
		});

		hashmap.insert("Apple tree", Plant {
			name: "Apple tree".to_string(),
			icon: 'A',
			color: Color::LGreen,

			blocks_passage: true,
			blocks_sight: true,
			size: Size::Huge,
			weight: Weight::Heavy,

			price: 450,

			harvest: Vec::new(),
		});

		PlantGen {
			hashmap
		}
	}
}

impl<'a> Generator for PlantGen<'a> {
	type Output = Plant;

	fn contains(&self, code: &str) -> bool {
		self.hashmap.contains_key(code)
	}

	fn code(&self, code: &str) -> Result<String, String> {
		if self.hashmap.contains_key(code) {
			Ok(code.to_string())
		}
		else {
			Err(format!("Code {} not valid in PlantGen", code))
		}
	}

	fn get(&self, code: &str) -> Plant {
		self.hashmap.get(code)
			.unwrap_or_else(|| panic!("Code {} not valid in PlantGen", code))
			.clone()
	}

	fn list(&self) -> Vec<&str> {
		let mut vec: Vec<&str> = Vec::with_capacity(self.hashmap.len());
		for key in self.hashmap.keys() {
			vec.push(key);
		}
		vec.sort();
		vec
	}
}