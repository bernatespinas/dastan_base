use crate::{
	Color, Size, Weight,
	Creature, ToCreature,
	Item, ToItem,
};

#[derive(Clone, Serialize, Deserialize)]
pub struct Corpse {
	name: String,
	pub icon: char,
	pub color: Color,

	pub blocks_passage: bool,
	pub blocks_sight: bool,

	pub price: u16,

	pub creature: Creature,
}

impl Corpse {
	pub fn new(creature: Creature) -> Corpse {
		Corpse {
			name: format!("{}'s corpse", creature.name()),
			icon: creature.icon(),
			color: creature.color(),

			blocks_passage: false,
			blocks_sight: false,

			price: 0,

			creature,
		}
	}
}

impl ToItem for Corpse {
	fn to_item(self) -> Item {
		Item::Corpse(Box::new(self))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn color(&self) -> Color {
		self.color
	}

	fn blocks_passage(&self) -> bool {
		self.blocks_passage
	}

	fn size(&self) -> Size {
		self.creature.size()
	}

	fn weight(&self) -> Weight {
		self.creature.weight()
	}

	fn price(&self) -> u16 {
		self.price
	}
}