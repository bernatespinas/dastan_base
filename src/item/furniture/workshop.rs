use std::collections::HashMap;

use crate::{
	Color, Size, Weight,
	Profession,
	Item, ToItem,
	Furniture, ToFurniture,
	Recipe,
	Inventory,
	GeneratorBundle,
};

// struct Progress {
// 	current: u8,
// 	total: u8,
// }

// impl Progress {
// 	fn new(total: u8) -> Progress {
// 		assert!(total >= 2);
// 		Progress {
// 			current: 1,
// 			total,
// 		}
// 	}
// }

#[derive(Clone, Serialize, Deserialize)]
pub struct Workshop {
	pub name: String,
	pub icon: char,
	pub color: Color,

	size: Size,
	weight: Weight,

	pub price: u16,

	pub profession: Profession,

	pub input: Inventory,
	pub output: Inventory,

	recipe: Option<Recipe>,
}

impl ToItem for Workshop {
	fn to_item(self) -> Item {
		Item::Furniture(box Furniture::Workshop(self))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn color(&self) -> Color {
		self.color
	}

	fn blocks_passage(&self) -> bool {
		true
	}

	fn size(&self) -> Size {
		self.size
	}

	fn weight(&self) -> Weight {
		self.weight
	}

	fn price(&self) -> u16 {
		self.price
	}
}

impl ToFurniture for Workshop {
	fn to_furniture(self) -> Furniture {
		Furniture::Workshop(self)
	}
}

impl Workshop {
	pub fn add_input(&mut self, item: Item) {
		self.input.push(item);
	}

	pub fn get_input(&mut self, index: usize) -> Item {
		self.input.remove(index)
	}

	pub fn get_output(&mut self, index: usize) -> Item {
		self.output.remove(index)
	}

	pub fn load_recipe(&mut self, recipe: &Recipe) {
		self.recipe = Some(recipe.clone());
	}

	pub fn clear_recipe(&mut self) {
		self.recipe = None;
	}

	pub fn craft(
		&mut self,
		recipe: &Recipe,
		gen_bundle: &GeneratorBundle
	) -> Result<(), String> {
		if let Ok(indexes) = self.input.check_all_present(&recipe.input) {
			self.input.remove_multiple(&indexes);
				self.output.push(
					gen_bundle.gen_item(&recipe.output)
				);
			// self.output = gen_bundle.gen_item(item);
			Ok(())
		}
		else {
			Err("Missing ingredients!".to_string())
		}
		// Err("The workshop is full!".to_string())
	}

	pub fn is_for_profession(&self, profession: Profession) -> bool {
		self.profession == profession
	}
}

pub fn insert_workshops(hashmap: &mut HashMap<&str, Furniture>) {
	hashmap.insert("Masonry", Workshop {
		name: "Masonry".to_string(),
		icon: 'W',
		color: Color::LGrey,

		size: Size::Huge,
		weight: Weight::SuperHeavy,

		price: 800,

		profession: Profession::Masonry,

		input: Inventory::new(10, Vec::new()),
		output: Inventory::new(10, Vec::new()),

		recipe: None,
	}.to_furniture());

	hashmap.insert("Carpentry", Workshop {
		name: "Carpentry".to_string(),
		icon: 'R',
		color: Color::DBrown,

		size: Size::Huge,
		weight: Weight::SuperHeavy,

		price: 700,

		profession: Profession::Carpentry,

		input: Inventory::new(10, Vec::new()),
		output: Inventory::new(10, Vec::new()),

		recipe: None,
	}.to_furniture());
}