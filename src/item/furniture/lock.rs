// TODO Don't use this here; move it to an action in engine or something. Only have here methods to attempt to unlock Locks with a given password or whatever.
// use crate::nrustes::{Window, TUIWindow, CTXWindow, Draw, Key};

use crate::Humanoid;

#[derive(Clone, Serialize, Deserialize)]
pub enum Lock {
	// Key and Keycard work the same, but have a different feeling...
	// Item, can be stolen.
	// Key(u16),
	// Item, can be stolen.
	// Keycard(u16),
	// Needs a finger (alive or dead (aka stolen)).
	// Fingerprint(u16),
	// Memory or tries/patience (-_-).
	Pin(u16),
	// ...
	Password(String),
	// Internet, being able to access an account from any Computer...
	UsernameAndPassword(String, String),
}

impl Lock {
	pub fn new_pin() -> Lock {
		Lock::Pin(Lock::enter_pin())
	}

	pub fn new_password() -> Lock {
		Lock::Password(Lock::enter_password())
	}

	// Option<Lock> because I'll usually put Lock inside an Option.
	// TODO Should I create a None variant?
	pub fn admin_rights(lock: Option<Lock>, _player: &Humanoid) -> bool {
		match lock {
			None							=> true,
			// Some(Lock::Key(dents))			=> dents == get_key(player).dents,
			// Some(Lock::Keycard(code))		=> code == get_keycard(player).code,
			Some(Lock::Pin(pin))			=> pin == Lock::enter_pin(),
			Some(Lock::Password(password))	=> password == Lock::enter_password(),
			Some(Lock::UsernameAndPassword(username, password))	=> username == Lock::enter_password() && password == Lock::enter_password(),
		}
	}

	pub fn enter_pin() -> u16 {
		// let win = Window::right("Enter PIN", "Esc - Back");
		// let device = Window::center_in(50, 30, &win);

		// device.print(2, 1, "9  8  7");
		// device.print(4, 1, "6  5  4");
		// device.print(6, 1, "3  2  1");
		// device.print(8, 1, "CN 0 OK");

		// let mut pin = String::with_capacity(4);
		// let mut i = 0;
		// while i < 4 {
		// 	device.print(10, 1, format!(">>> {}", pin));
		// 	let key = device.input_key_normal();

		// 	if let Key::Char(c) = key {
		// 		Window::error(&format!("c is {}", c));
		// 		if c.to_string().parse::<u16>().is_ok() {
		// 			pin.push(c);
		// 			Window::error(&format!("added {}, now pin is {}", c, pin));
		// 			i += 1;
		// 		}
		// 	}
		// }

		// pin.parse::<u16>().unwrap()
		5556
	}

	fn enter_password() -> String {
		// let win = Window::right("Enter password", "Esc - Back");
		// let device = Window::center_in(50, 30, &win);

		// device.print(2, 1, "1 2 3 4 5 6 7 8 9 0");
		// device.print(4, 1, "q w e r t y u i o p");
		// device.print(6, 1, "a s d f g h j k l ñ");
		// device.print(8, 1, "z x c v b n m . - _");

		// device.input_string(10, 1, ">>> ")
		"potate".to_string()
	}
}

// fn get_key(player: &Humanoid) -> __ {

// }

// // TODO Huh, maybe I could simply pass the enum variant I'm interested in and match it in inventory_picker...
// fn filter_keycard(item: &Item) -> bool {
// 	match item {
// 		Item::Security(Security::Key(..))	=> true,
// 		_									=> false,
// 	}
// }

// fn get_keycard(player: &Humanoid) -> &Key {
// 	match inventory_picker(player) {
// 		Some(pos)	=> match player.inventory.ref_item(pos) {
// 			Item::Security(Security::Key(key))	=> &key,
			
// 		}
// 	}
// }