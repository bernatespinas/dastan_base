use std::collections::HashMap;

use crate::{Item, ToItem, Furniture, ToFurniture, Material, Color, Size, Weight};

const BED: char = 'Û';

#[derive(Clone, Serialize, Deserialize)]
pub struct Bed {
	pub name: String,
	pub icon: char,
	pub color: Color,

	pub blocks_passage: bool,
	blocks_sight: bool,
	size: Size,
	weight: Weight,

	pub price: u16,

	material: Material,

	comfy: bool,
}

impl ToFurniture for Bed {
	fn to_furniture(self) -> Furniture {
		Furniture::Bed(self)
	}
}

impl ToItem for Bed {
	fn to_item(self) -> Item {
		Item::Furniture(Box::new(self.to_furniture()))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn color(&self) -> Color {
		self.color
	}

	fn blocks_passage(&self) -> bool {
		self.blocks_passage
	}

	fn size(&self) -> Size {
		self.size
	}

	fn weight(&self) -> Weight {
		self.weight
	}

	fn price(&self) -> u16 {
		self.price
	}
}

pub fn insert_beds(hashmap: &mut HashMap<&str, Furniture>) {
	hashmap.insert("BED_WOOD", Bed {
		name: "Wooden bed".to_string(),
		icon: BED,
		color: Color::DBrown,

		blocks_passage: false,
		blocks_sight: false,
		size: Size::Big,
		weight: Weight::Heavy,

		price: 250,

		material: Material::Wood,

		comfy: true,
	}.to_furniture());
}