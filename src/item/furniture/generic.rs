use std::collections::HashMap;

use crate::{Item, ToItem, Furniture, ToFurniture, Material, Color, Size, Weight};

const CHAIR: char = 'Ú';
const TABLE: char = 'Ù';

#[derive(Clone, Serialize, Deserialize)]
pub struct Generic {
	pub name: String,
	pub icon: char,
	pub color: Color,

	pub blocks_passage: bool,
	blocks_sight: bool,
	size: Size,
	weight: Weight,

	pub price: u16,

	material: Material,
}

impl ToFurniture for Generic {
	fn to_furniture(self) -> Furniture {
		Furniture::Generic(self)
	}
}

impl ToItem for Generic {
	fn to_item(self) -> Item {
		Item::Furniture(Box::new(self.to_furniture()))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn color(&self) -> Color {
		self.color
	}

	fn blocks_passage(&self) -> bool {
		self.blocks_passage
	}

	fn size(&self) -> Size {
		self.size
	}

	fn weight(&self) -> Weight {
		self.weight
	}

	fn price(&self) -> u16 {
		self.price
	}
}

pub fn insert_generics(hashmap: &mut HashMap<&str, Furniture>) {
	hashmap.insert("TABLE_WOOD", Generic {
		name: "Wooden table".to_string(),
		icon: TABLE,
		color: Color::LBrown,

		blocks_passage: true,
		blocks_sight: false,
		size: Size::Medium,
		weight: Weight::Heavy,

		price: 45,

		material: Material::Wood,
	}.to_furniture());

	hashmap.insert("CHAIR_WOOD", Generic {
		name: "Wooden chair".to_string(),
		icon: CHAIR,
		color: Color::LBrown,

		blocks_passage: false,
		blocks_sight: false,
		size: Size::Medium,
		weight: Weight::Medium,

		price: 35,

		material: Material::Wood,
	}.to_furniture());

	hashmap.insert("LAMP", Generic {
		name: "Broken lamp".to_string(),
		icon: 'T',
		color: Color::Yellow,

		blocks_passage: true,
		blocks_sight: false,
		size: Size::Medium,
		weight: Weight::Medium,

		price: 75,

		material: Material::Iron,
	}.to_furniture());
}