use crate::base::items::Resource;

#[derive(Clone, Serialize, Deserialize)]
pub struct Blueprint {
	pub name: String,
	pub icon: char,
	pub color: Color,

	pub price: u16,

	pub blocks_passage: bool,

	// pub furniture_to_craft: Option<String>,
	pub recipe: Option<Recipe>,
	pub input: Inventory,
}

impl ToFurniture for Blueprint {
	fn to_furniture(self) -> Furniture {
		Furniture::Blueprint(self)
	}
}

impl ToItem for Blueprint {
	fn to_item(self) -> Item {
		Item::Furniture(Box::new(self.to_furniture()))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn color(&self) -> Color {
		self.color
	}

	fn blocks_passage(&self) -> bool {
		self.blocks_passage
	}

	fn price(&self) -> u16 {
		self.price
	}
}

impl Blueprint {
	
}