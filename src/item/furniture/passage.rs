use std::collections::HashMap;

use uuid::Uuid;

use crate::{Item, ToItem, Furniture, ToFurniture, Lock, Toggle, Material, Color, Size};
use super::{Device, DeviceId, Weight};

const DOOR: char = 'û';
const WINDOW: char = 'ü';

#[derive(Clone, Serialize, Deserialize)]
pub enum TeleportLocation {
	// TODO At what ZonePositon? I use 0,0 for now.
	Map(String),
	AtDevice(DeviceId),
	NextToDevice(DeviceId),
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Passage {
	pub name: String,

	icon_open: char,
	color_open: Color,
	icon_closed: char,
	color_closed: Color,

	pub blocks_passage: bool,
	blocks_sight: bool,

	pub price: u16,

	material: Material,

	device_id: Option<Uuid>,

	/// A Passage may offer to teleport the player to another locaton.
	pub teleport_location: Option<TeleportLocation>,

	lock: Option<Lock>,
	// Pros: If I deconstruct a linked Passage, I know from which Computer to unlink it to/from.
	// But should connected/linked devices be able to get deconstructed?
	// linked_by: Option<Position>,
	// Pros: It shouldn't be possible to deconstruct a device that's linked. I don't care to which device, right?
	// If I deconstruct a Computer, I only deconstruct the machine, so the "account" still exists. When deleting an account, linked devices could then be unlinked or something...
	// TODO Hmm, is it really worth the trouble? Hmm...
	linked: bool,
}

impl ToFurniture for Passage {
	fn to_furniture(self) -> Furniture {
		Furniture::Passage(self)
	}
}

impl ToItem for Passage {
	fn to_item(self) -> Item {
		Item::Furniture(Box::new(self.to_furniture()))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		if self.blocks_passage {
			self.icon_closed
		}
		else {
			self.icon_open
		}
	}

	fn color(&self) -> Color {
		if self.blocks_passage {
			self.color_closed
		}
		else {
			self.color_open
		}
	}

	fn blocks_passage(&self) -> bool {
		self.blocks_passage
	}

	fn size(&self) -> Size {
		Size::Big
	}

	fn weight(&self) -> Weight {
		Weight::Heavy
	}

	fn price(&self) -> u16 {
		self.price
	}
}

impl Device for Passage {
	fn device_id(&self) -> Option<DeviceId> {
		self.device_id.map(|uuid| DeviceId::Passage(uuid))
	}

	fn set_random_device_id(mut self) -> Passage {
		self.device_id = Some(Uuid::new_v4());
		self
	}
}

impl Toggle for Passage {
	fn toggle(&mut self) {
		self.blocks_passage = !self.blocks_passage;
	}

	// fn can_be_linked(&self) -> bool {
	// 	!self.linked
	// }

	fn can_be_linked(&self) -> Result<(), String> {
		if self.linked {
			Err("This device is already linked!".to_string())
		}
		else {
			Ok(())
		}
	}

	fn link(&mut self) {
		self.linked = true;
	}
}

// impl Passage {
// 	pub fn set_location(&mut self, location: TeleportLocation) {
// 		self.location = Some(location);
// 	}
// }

pub fn insert_passages(hashmap: &mut HashMap<&str, Furniture>) {
	hashmap.insert("DOOR_WOOD", Passage {
		name: "Wooden door".to_string(),
		icon_open: '_',
		color_open: Color::DBrown,
		icon_closed: DOOR,
		color_closed: Color::DBrown,

		blocks_passage: false,
		blocks_sight: true,

		price: 25,

		material: Material::Wood,

		device_id: None,
		teleport_location: None,
		lock: None,
		linked: false,
	}.to_furniture());

	hashmap.insert("WINDOW_WOOD", Passage {
		name: "Wooden window".to_string(),
		icon_open: WINDOW,
		color_open: Color::DBrown,
		icon_closed: WINDOW,
		color_closed: Color::LBrown,

		blocks_passage: true,
		blocks_sight: false,

		price: 25,

		material: Material::Wood,

		device_id: None,
		teleport_location: None,
		lock: None,
		linked: false,
	}.to_furniture());

	hashmap.insert("PLANET_TELEPORTER", Passage {
		name: "Planet teleporter".to_string(),
		icon_open: '+',
		color_open: Color::LGreen,
		icon_closed: '-',
		color_closed: Color::Red,

		blocks_passage: false,
		blocks_sight: false,

		price: 995,

		material: Material::Iron,

		device_id: None,
		teleport_location: None,
		lock: None,
		linked: false
	}.to_furniture());
}