use std::collections::HashMap;

use uuid::Uuid;

use crate::{Item, ToItem, Furniture, ToFurniture, Color, Size, Weight};
use super::{Device, DeviceId};

const COMPUTER: char = 'Ś';

// TODO: CD, DVD, USB... I like CDs' feel.
#[derive(Clone, Serialize, Deserialize)]
pub struct CdRom {}

#[derive(Clone, Serialize, Deserialize)]
pub struct Computer {
	pub name: String,
	pub icon: char,
	pub color: Color,

	pub blocks_passage: bool,
	blocks_sight: bool,
	size: Size,
	weight: Weight,

	pub price: u16,

	device_id: Option<Uuid>,

	pin: [u8; 4],
	cdrom: Option<CdRom>,

	internet: bool,
}

impl ToFurniture for Computer {
	fn to_furniture(self) -> Furniture {
		Furniture::Computer(self)
	}
}

impl ToItem for Computer {
	fn to_item(self) -> Item {
		Item::Furniture(Box::new(self.to_furniture()))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn color(&self) -> Color {
		self.color
	}

	fn blocks_passage(&self) -> bool {
		self.blocks_passage
	}

	fn size(&self) -> Size {
		self.size
	}

	fn weight(&self) -> Weight {
		self.weight
	}

	fn price(&self) -> u16 {
		self.price
	}
}

impl Device for Computer {
	fn device_id(&self) -> Option<DeviceId> {
		self.device_id.map(|uuid| DeviceId::Computer(uuid))
	}

	fn set_random_device_id(mut self) -> Computer {
		self.device_id = Some(Uuid::new_v4());
		self
	}
}

pub fn insert_computers(hashmap: &mut HashMap<&str, Furniture>) {
	hashmap.insert("COMPUTER", Computer {
		name: "Computer".to_string(),
		icon: COMPUTER,
		color: Color::DGrey,

		blocks_passage: true,
		blocks_sight: false,

		price: 1500,

		size: Size::Medium,
		weight: Weight::Medium,
		device_id: None,
		
		pin: [0, 0, 0, 0],
		cdrom: None,

		internet: false,
	}.to_furniture());
}