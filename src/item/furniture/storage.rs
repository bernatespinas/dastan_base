use std::collections::HashMap;

use crate::{Inventory, Item, ToItem, Furniture, ToFurniture, Lock, Material, Color, Size, Weight};

const CHEST: char = 'á';

// Cupboard, shelf, chest...
#[derive(Clone, Serialize, Deserialize)]
pub struct Storage {
	pub name: String,
	pub icon: char,
	pub color: Color,

	pub blocks_passage: bool,
	blocks_sight: bool,
	size: Size,
	weight: Weight,

	pub price: u16,

	material: Material,

	lock: Option<Lock>,
	pub inventory: Inventory,
}

impl ToFurniture for Storage {
	fn to_furniture(self) -> Furniture {
		Furniture::Storage(self)
	}
}

impl ToItem for Storage {
	fn to_item(self) -> Item {
		Item::Furniture(Box::new(self.to_furniture()))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn color(&self) -> Color {
		self.color
	}

	fn blocks_passage(&self) -> bool {
		self.blocks_passage
	}

	fn size(&self) -> Size {
		self.size
	}

	fn weight(&self) -> Weight {
		self.weight
	}

	fn price(&self) -> u16 {
		self.price
	}
}

pub fn insert_storages(hashmap: &mut HashMap<&str, Furniture>) {
	hashmap.insert("CHEST_WOOD", Storage {
		name: "Wooden chest".to_string(),
		icon: CHEST,
		color: Color::LBrown,

		blocks_passage: true,
		blocks_sight: false,
		size: Size::Medium,
		weight: Weight::Medium,

		price: 30,

		material: Material::Wood,

		lock: None,
		inventory: Inventory::with_capacity(5),
	}.to_furniture());
}