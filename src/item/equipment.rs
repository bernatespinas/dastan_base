use std::collections::HashMap;

use crate::{Item, ToItem, OrganicSlot, Material, Color, Size, Weight, Generator};

// Armor.
const HELMET: char = 'È';
// const SHIRT: char = 'Ê';
// const TROUSERS: char = 'É';
// const BOOTS: char = 'Ë';

// Jewelry.
// const RING: char = 'é';
// const NECKLACE: char = '?';
// const AMULET: char = '?';
// const BRACELET: char = '?';

// Weapons.
const SWORD: char = 'ò';
// const DAGGER: char = 'ö';
// const BOW: char = '?';
// const ARROW: char = '?';

#[derive(Clone, Serialize, Deserialize)]
pub struct Equipment {
	pub name: String,
	pub icon: char,
	pub color: Color,

	pub price: u16,

	pub vitality: u16,
	pub strength: u16,
	pub defense: u16,

	pub slot: OrganicSlot,
	pub material: Material,
}

impl ToItem for Equipment {
	fn to_item(self) -> Item {
		Item::Equipment(Box::new(self))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn color(&self) -> Color {
		self.color
	}

	fn blocks_passage(&self) -> bool {
		false
	}

	fn size(&self) -> Size {
		Size::Small
	}

	fn weight(&self) -> Weight {
		Weight::Light
	}

	fn price(&self) -> u16 {
		self.price
	}
}

pub struct EquipmentGen<'a> {
	hashmap: HashMap<&'a str, Equipment>,
}

impl<'a> EquipmentGen<'a> {
	pub fn generate() -> EquipmentGen<'a> {
		let mut hashmap: HashMap<&str, Equipment> = HashMap::new();

		hashmap.insert("HELMET_LEATHER", Equipment {
			name: "Leather helmet".to_string(),
			icon: HELMET,
			color: Color::Orange,

			price: 20,

			vitality: 10,
			strength: 0,
			defense: 10,

			slot: OrganicSlot::Head,
			material: Material::Leather,
		});

		hashmap.insert("SWORD_IRON", Equipment {
			name: "Iron sword".to_string(),
			icon: SWORD,
			color: Color::DGrey,

			price: 20,

			vitality: 5,
			strength: 20,
			defense: 2,

			slot: OrganicSlot::Weapon,
			material: Material::Iron,
		});

		EquipmentGen {
			hashmap,
		}
	}
}

impl<'a> Generator for EquipmentGen<'a> {
	type Output = Equipment;

	fn contains(&self, code: &str) -> bool {
		self.hashmap.contains_key(code)
	}

	fn code(&self, code: &str) -> Result<String, String> {
		if self.hashmap.contains_key(code) {
			Ok(code.to_string())
		}
		else {
			Err(format!("Code {} not valid in EquipmentGen", code))
		}
	}

	fn get(&self, code: &str) -> Equipment {
		self.hashmap.get(code)
			.unwrap_or_else(|| panic!("Code {} not valid in EquipmentGen", code))
			.clone()
	}

	fn list(&self) -> Vec<&str> {
		let mut vec: Vec<&str> = Vec::with_capacity(self.hashmap.len());
		for key in self.hashmap.keys() {
			vec.push(key);
		}
		vec.sort();
		vec
	}
}