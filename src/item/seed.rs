use crate::{Item, ToItem, Color, Size, Weight};

#[derive(Clone, Serialize, Deserialize)]
pub struct Seed {
	pub name: String,
	pub icon: char,
	pub color: Color,

	pub price: u16,

	// water_level: u8,
	pub plant_name: String,
}

impl ToItem for Seed {
	fn to_item(self) -> Item {
		Item::Seed(Box::new(self))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn color(&self) -> Color {
		self.color
	}

	fn blocks_passage(&self) -> bool {
		false
	}

	fn size(&self) -> Size {
		Size::Tiny
	}

	fn weight(&self) -> Weight {
		Weight::Light
	}

	fn price(&self) -> u16 {
		self.price
	}
}