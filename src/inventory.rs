use std::collections::HashSet;

use crate::{Item, ToItem, Size, Weight};

#[derive(Clone, Serialize, Deserialize)]
pub struct Inventory {
	capacity: usize,
	items: Vec<Item>,
}

impl Inventory {
	pub fn new(capacity: usize, items: Vec<Item>) -> Inventory {
		assert!(items.len() <= capacity, "capacity > items.len()");
// 		if items.len() > capacity {
// 			panic!("capacity > items.len()")
// 		}

		Inventory {
			capacity,
			items,
		}
	}

	pub fn with_capacity(capacity: usize) -> Inventory {
		Inventory {
			capacity,
			items: Vec::new(),
		}
	}

	pub fn items(&self) -> &Vec<Item> {
		&self.items
	}

	pub fn iter(&self) -> impl Iterator<Item=&Item> {
		self.items.iter()
	}

	pub fn len(&self) -> usize {
		self.items.len()
	}

	pub fn capacity(&self) -> usize {
		self.capacity
	}

	pub fn is_empty(&self) -> bool {
		self.items.is_empty()
	}

	pub fn is_full(&self) -> bool {
		self.items.len() >= self.capacity
	}

	pub fn has_space_left(&self, amount: usize) -> bool {
		self.len() + amount <= self.capacity
	}

	pub fn increase_capacity(&mut self, amount: usize) {
		self.capacity += amount;
	}

	pub fn decrease_capacity(&mut self, amount: usize) {
		if self.capacity > self.len() {
			self.capacity -= amount;
		}
		else {
			panic!("Tried to decrease_capacity of Inventory when self.capacity <= self.len().");
		}
	}

// 	pub fn has_items(&self, items: Vec<Item<'a>>) -> bool {
// 		TODO
// 	}

	pub fn insert(&mut self, item: Item, index: usize) {
		self.items.insert(index, item);
	}

	pub fn push(&mut self, item: Item) {
		self.items.push(item);
	}

	pub fn can_pick_up(&self, item: &Item) -> Result<(), String> {
		if self.is_full() {
			Err("The inventory is full!".to_string())
		} else if item.size() > Size::Small {
			Err("The item is too big!".to_string())
		} else if item.weight() > Weight::Light {
			Err("The item is too heavy!".to_string())
		} else {
			Ok(())
		}
	}

	// TODO: Hmm, I'd like to check here if there's space left for the item. But if there isn't and I return the item, I'd have to put it back in the map (because I'd have had extracted it from there), which is a bit weird.
	// TODO: Is pick_up okay? Which name to choose?
	// TODO: Should I just push the thing? Making the check manually before.
	pub fn pick_up<T>(&mut self, item: T) -> Result<(), Item>
	where
		T: ToItem,
	{
		if self.items.len() >= self.capacity {
			Err(item.to_item())
		}
		else {
			self.items.push(item.to_item());
			Ok(())
		}
	}

	pub fn remove(&mut self, index: usize) -> Item {
		self.items.remove(index)
	}

	pub fn remove_multiple(&mut self, indexes: &HashSet<usize>) -> Vec<Item> {
		// Since HashSet doesn't/can't have .iter().rev(), I guess I'll have to use a Vec...
		use std::iter::FromIterator;
		let indexes_vec: Vec<&usize> = Vec::from_iter(indexes.iter());

		// I reverse the iterator because I have to remove Items from the Vec<Item> starting from the end in order to preserve their..., ¿order? The way they are sorted.
		// If I have to remove the Items at indexes 0 and 1 and start at the beginning, I remove Item 0 and then Item 1 becomes Item 0. Then, I remove Item 1, which originally was Item 2.
		indexes_vec.iter().rev()
			.map(|i| self.remove(**i))
			.collect()

		// Alternative, not as cool, version.
		// let mut removed: Vec<Item> = Vec::with_capacity(indexes.len());

		// for i in indexes.iter().rev() {
		// 	removed.push(self.remove(*i))
		// }

		// removed
	}

	pub fn ref_item(&self, index: usize) -> &Item {
		self.items.get(index).unwrap()
	}

	pub fn mut_item(&mut self, index: usize) -> &mut Item {
		self.items.get_mut(index).unwrap()
	}

	/// Checks if/that all the items' names ___ are present in self.items. Returns Ok with a Vec containing the indexes of the items that are present ___; Err otherwise. Compares by name.
	// If __ can be crafted, returns Ok(Vec<usize>) with the indexes of the items that will get removed.
	// TODO Changed from &Vec<&str> to &Vec<String> because Recipe.input is now Vec<String>. Use Cow or something?
	pub fn check_all_present(&self, names: &[String]) -> Result<HashSet<usize>, ()> {
		let mut items_indexes: HashSet<usize> = HashSet::with_capacity(names.len());

		for name in names {
			let mut found = false;
			'search: for (index, item) in self.items.iter().enumerate() {
				// TODO When I changed to input: &Vec<&str> from input: &Vec<String>, the compailer complained about this:
//    --> base/./src/inventory.rs:121:19
//     |
// 121 |                 if name == item.name() {
//     |                               ^^ no implementation for `&str == str`
//     |
//     = help: the trait `std::cmp::PartialEq<str>` is not implemented for `&str`
//     = note: required because of the requirements on the impl of `std::cmp::PartialEq<&str>` for `&&str`
				// I put a & in front of iten_inv.name() and TADAA!! It weerks.
				// if name == &item.name() {
				// TODO Clippy told me to remove the &
				if name == item.name() {
					if items_indexes.contains(&index) {
						continue 'search;
					} else {
						items_indexes.insert(index);
						// continue 'recipe;
						found = true;
					}
				}
			}
			// *sigh* This /name/ is not in inventory! -_-
			if !found {
				return Err(());
			}
		}

		Ok(items_indexes)
	}

	// pub fn check_all_present_hashmap(&self, items: &HashMap<String, usize>) -> Result<Vec<usize>, ()> {
	// 	use std::iter::FromIterator;

	// 	TODO omg...... cloning.......
	// 	let mut items2: HashMap<&String, &usize> = HashMap::from_iter(items.iter());

	// 	for item in self.items {
	// 		if items2.contains_key(item.name()) {
	// 			items2.get_mut(item.name())
	// 		}
	// 	}
	// }
}

// pub trait Loot {
	// fn loot/get_inventory()/??
// }