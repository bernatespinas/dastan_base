use nrustes::Color;

use crate::{Size, Weight, Generator, ItemBundle};

pub mod humanoid;
use humanoid::{Humanoid, HumanoidGen};
pub mod animal;
use animal::{Animal, AnimalGen};
pub mod spaceship;
use spaceship::{Spaceship, SpaceshipGen};
pub mod body;

#[derive(Clone, Serialize, Deserialize)]
pub enum Creature {
	Humanoid(Box<humanoid::Humanoid>),
	Animal(Box<animal::Animal>),
	Spaceship(Box<spaceship::Spaceship>),
}

impl Creature {
	pub fn humanoid(&self) -> Result<&Box<Humanoid>, ()> {
		if let Creature::Humanoid(humanoid) = self {
			Ok(humanoid)
		} else {
			Err(())
		}
	}

	pub fn humanoid_mut(&mut self) -> Result<&mut Box<Humanoid>, ()> {
		if let Creature::Humanoid(humanoid) = self {
			Ok(humanoid)
		} else {
			Err(())
		}
	}

	pub fn humanoid_take(self) -> Result<Humanoid, ()> {
		if let Creature::Humanoid(humanoid) = self {
			Ok(*humanoid)
		} else {
			Err(())
		}
	}
}

pub trait ToCreature {
	fn to_creature(self) -> Creature;
	fn name(&self) -> &str;
	fn icon(&self) -> char;
	fn color(&self) -> Color;

	fn size(&self) -> Size;
	fn weight(&self) -> Weight;

	fn vitality(&self) -> u16;
	fn strength(&self) -> u16;
	fn defense(&self) -> u16;

	fn is_alive(&self) -> bool;
	
	fn attack<T>(&mut self, enemy: &mut T) -> String
	where
		T: ToCreature,
	{
		let damage =
			if self.strength() > enemy.defense() {
				self.strength() - enemy.defense()
			}
			else {
				enemy.defense() - self.strength()
			}
		;
		enemy.take_damage(damage);

		if enemy.is_alive() {
			return format!("{} attacks {} for {} damage.",
						self.name(), enemy.name(), damage);
		}
		else {
			return format!("{} attacks and kills {} for {} damage!",
						self.name(), enemy.name(), damage);
		}
	}

	fn take_damage(&mut self, damage: u16) -> String;
	fn die(&mut self) -> String;
	fn hp(&self) -> u16;
}

impl ToCreature for Creature {
	fn to_creature(self) -> Creature {
		self
	}

	fn name(&self) -> &str {
		match self {
			Creature::Humanoid(humanoid)	=> humanoid.name(),
			Creature::Animal(animal)		=> animal.name(),
			Creature::Spaceship(spaceship)	=> spaceship.name(),
		}
	}

	fn icon(&self) -> char {
		match self {
			Creature::Humanoid(humanoid)	=> humanoid.icon(),
			Creature::Animal(animal)		=> animal.icon(),
			Creature::Spaceship(spaceship)	=> spaceship.icon(),
		}
	}

	fn color(&self) -> Color {
		match self {
			Creature::Humanoid(humanoid)	=> humanoid.color(),
			Creature::Animal(animal)		=> animal.color(),
			Creature::Spaceship(spaceship)	=> spaceship.color(),
		}
	}

	fn size(&self) -> Size {
		match self {
			Creature::Humanoid(humanoid)	=> humanoid.size(),
			Creature::Animal(animal)		=> animal.size(),
			Creature::Spaceship(spaceship)	=> spaceship.size(),
		}
	}

	fn weight(&self) -> Weight {
		match self {
			Creature::Humanoid(humanoid)	=> humanoid.weight(),
			Creature::Animal(animal)		=> animal.weight(),
			Creature::Spaceship(spaceship)	=> spaceship.weight(),
		}
	}

	fn vitality(&self) -> u16 {
		match self {
			Creature::Humanoid(humanoid) => humanoid.vitality(),
			Creature::Animal(animal) => animal.vitality(),
			Creature::Spaceship(spaceship) => spaceship.vitality(),
		}
	}

	fn strength(&self) -> u16 {
		match self {
			Creature::Humanoid(humanoid) => humanoid.strength(),
			Creature::Animal(animal) => animal.strength(),
			Creature::Spaceship(spaceship) => spaceship.strength(),
		}
	}

	fn defense(&self) -> u16 {
		match self {
			Creature::Humanoid(humanoid) => humanoid.defense(),
			Creature::Animal(animal) => animal.defense(),
			Creature::Spaceship(spaceship) => spaceship.defense(),
		}
	}
	
	fn is_alive(&self) -> bool {
		match self {
			Creature::Humanoid(humanoid)	=> humanoid.is_alive(),
			Creature::Animal(animal)		=> animal.is_alive(),
			Creature::Spaceship(spaceship)	=> spaceship.is_alive(),
		}
	}

	fn take_damage(&mut self, damage: u16) -> String {
		match self {
			Creature::Humanoid(humanoid)	=> humanoid.take_damage(damage),
			Creature::Animal(animal)		=> animal.take_damage(damage),
			Creature::Spaceship(spaceship)	=> spaceship.take_damage(damage),
		}
	}

	fn die(&mut self) -> String {
		match self {
			Creature::Humanoid(humanoid)	=> humanoid.die(),
			Creature::Animal(animal)		=> animal.die(),
			Creature::Spaceship(spaceship)	=> spaceship.die(),
		}
	}

	fn hp(&self) -> u16 {
		match self {
			Creature::Humanoid(humanoid)	=> humanoid.hp(),
			Creature::Animal(animal)		=> animal.hp(),
			Creature::Spaceship(spaceship)	=> spaceship.hp(),
		}
	}
}

#[derive(Clone, Serialize, Deserialize)]
pub struct QuestLog {
	reputation: u16,
	quests: Vec<Quest>,
}

impl Default for QuestLog {
	fn default() -> QuestLog {
		QuestLog {
			reputation: 0,
			quests: Vec::new(),
		}
	}
}

impl QuestLog {
	pub fn add_quest(&mut self, quest: Quest) {
		self.quests.push(quest);
	}

	pub fn del_quest(&mut self, index: usize) -> Quest {
		self.quests.remove(index)
	}

	// TODO: So... If I unwrap() the thing, I make sure that every call to the function has a valid index, BUT, that waaay..., if the quest log is eeeemptyy..., I can't do the "if let Some" thing, so... What's better? Inventory does it without the Option, with the unwrap...
	pub fn ref_quest(&self, index: usize) -> Option<&Quest> {
		self.quests.get(index)
	}

	pub fn mut_quest(&mut self, index: usize) -> Option<&mut Quest> {
		self.quests.get_mut(index)
	}

	// TODO: Call Quest::complete?
	pub fn complete_quest(&mut self, index: usize) {
		self.mut_quest(index).unwrap().completed = true;
	}

	// fn complete_quest(&mut self, i: usize) {
	//
	// }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Quest {
	pub objective: String,
	pub reward: String,
	pub completed: bool,
}

pub struct CreatureBundle<'a> {
	pub humanoid_gen: HumanoidGen<'a>,
	pub animal_gen: AnimalGen<'a>,
	pub spaceship_gen: SpaceshipGen<'a>,
}

impl<'a> CreatureBundle<'a> {
	pub fn new(item_bundle: &ItemBundle<'a>) -> CreatureBundle<'a> {
		let humanoid_gen = HumanoidGen::generate(item_bundle);
		let animal_gen = AnimalGen::generate();
		let spaceship_gen = SpaceshipGen::generate();

		CreatureBundle {
			humanoid_gen,
			animal_gen,
			spaceship_gen,
		}
	}

	pub fn gen_creature(&self, code: &str) -> Creature {
		if self.spaceship_gen.contains(code) {
			self.spaceship_gen.get(code).to_creature()
		}
		else if self.animal_gen.contains(code) {
			self.animal_gen.get(code).to_creature()
		}
		else if self.humanoid_gen.contains(code) {
			self.humanoid_gen.get(code).to_creature()
		}
		else {
			panic!("code {} not found", code);
		}
	}

	pub fn gen_humanoid(&self, code: &str) -> Humanoid {
		self.humanoid_gen.get(code)
	}

	pub fn gen_animal(&self, code: &str) -> Animal {
		self.animal_gen.get(code)
	}

	pub fn gen_spaceship(&self, code: &str) -> Spaceship {
		self.spaceship_gen.get(code)
	}
}