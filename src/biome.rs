use std::collections::HashMap;

use crate::{Generator, TerrainBundle, CreatureBundle, ItemBundle};
use crate::time::Season;

// enum Biome {
// 	Jungle,
// 	Forest,
// 	Savannah,
// 	Desert,
// 	Beach,
	// IceSteppes,
	// FrozenPlains,
	// Tundra,
// 	Ocean,
// 	Sea,
// 	// Lake,
// 	// River,
// 	SeaDepths,
// 	SeaBottom,
// 	Space,
// }

// enum WeatherFenomenos {
// 	Rain,

// }

// enum Sky {
// 	// Clear means no clouds or any other thing. If it's daytime, it will be Sunny, and if it's nighttime, uh, Moony (-_- ???? gibu apu already).
// 	Clear,
// 	Cloudy,
// 	Rain,
// 	Storm,
// 	Thunder,
// 	Thunderstorm,
// }

// The "state" of the wind. It's independent of Sky. 
// TODO Strength of the wind or something...? Like, should I have to "calculate"/change it every turn or every hour? Well, I added some strength variants.  
// enum Wind {
// 	Calm,
// 	WeakWind, //TODO Breeze. Breezy? >.<
// 	InTheMiddleWind,
// 	StrongWind,
// }

#[derive(Clone, Serialize, Deserialize)]
pub struct Biome {
	name: String,

	// fauna: SeasonChance,
	pub flora: SeasonChance,
	pub terrain: String,

	// sky_chance: HashMap<Season, HashMap<PartOfDay, u8>>,
	// wind_chance: HashMap<Season, HashMap<PartOfDay, HashMap<Wind, u8>>>,
	// wind_chance: u8,

	// sky: Sky,
	// wind: Wind,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct SeasonChance {
	seasons: HashMap<Season, HashMap<(u8, u8), String>>,
}

impl Default for SeasonChance {
	fn default() -> SeasonChance {
		let mut seasons = HashMap::new();
		seasons.insert(Season::Winter, HashMap::new());
		seasons.insert(Season::Spring, HashMap::new());
		seasons.insert(Season::Summer, HashMap::new());
		seasons.insert(Season::Autumn, HashMap::new());

		SeasonChance {
			seasons
		}
	}
}

impl SeasonChance {
	fn with_chances(chances: Vec<((u8, u8), String)>) -> SeasonChance {
		let mut season_chance = SeasonChance::default();

		for (chance, what) in chances {
			season_chance.seasons.get_mut(&Season::Winter).unwrap().insert(chance, what.clone());
			season_chance.seasons.get_mut(&Season::Spring).unwrap().insert(chance, what.clone());
			season_chance.seasons.get_mut(&Season::Summer).unwrap().insert(chance, what.clone());
			season_chance.seasons.get_mut(&Season::Autumn).unwrap().insert(chance, what.clone());
		}

		season_chance
	}

	pub fn get_species<T: Generator>(
		&self,
		season: Season,
		gen: &T,
		chance_num: u8
	) -> Option<T::Output> {
		for (chance, what) in self.seasons.get(&season).unwrap() {
			if chance.0 <= chance_num && chance_num <= chance.1 {
				return Some(gen.get(what));
			}
		}

		None
	}
}

pub struct BiomeGen<'a> {
	hashmap: HashMap<&'a str, Biome>,
}

impl<'a> BiomeGen<'a> {
	pub fn generate(
		terrain_bundle: &TerrainBundle,
		_creature_bundle: &CreatureBundle,
		item_bundle: &ItemBundle
	) -> Self {
		let mut hashmap: HashMap<&str, Biome> = HashMap::new();

		hashmap.insert("FOREST_TEMPERATE", Biome {
			name: "Temperate forest".to_string(),

			terrain: terrain_bundle.foundation_gen.code("FLOOR_GRASS").unwrap(),
			flora: SeasonChance::with_chances(vec!(
				((0, 2), item_bundle.plant_gen.code("Pine").unwrap())
			)),
		});

		hashmap.insert("ICE_STEPPES", Biome {
			name: "Ice steppes".to_string(),

			terrain: terrain_bundle.foundation_gen.code("FLOOR_ICE").unwrap(),
			flora: SeasonChance::default(),
		});

		hashmap.insert("SEA_DEPTHS", Biome {
			name: "Sea depths".to_string(),

			terrain: terrain_bundle.liquid_gen.code("Deep salt water").unwrap(),
			flora: SeasonChance::default(),
		});

		hashmap.insert("SPACE", Biome {
			name: "Space".to_string(),

			terrain: terrain_bundle.foundation_gen.code("VOID").unwrap(),
			flora: SeasonChance::default()
		});

		BiomeGen {
			hashmap,
		}
	}
}

impl<'a> Generator for BiomeGen<'a> {
	type Output = Biome;
	
	fn contains(&self, code: &str) -> bool {
		self.hashmap.contains_key(code)
	}

	fn code(&self, code: &str) -> Result<String, String> {
		if self.hashmap.contains_key(code) {
			Ok(code.to_string())
		}
		else {
			Err(format!("Code {} not valid in BiomeGen", code))
		}
	}

	fn get(&self, code: &str) -> Biome {
		self.hashmap.get(code)
			.unwrap_or_else(|| panic!("Code {} not valid in BiomeGen", code))
			.clone()
	}

	fn list(&self) -> Vec<&str> {
		let mut vec: Vec<&str> = Vec::with_capacity(self.hashmap.len());
		for key in self.hashmap.keys() {
			vec.push(key);
		}
		vec.sort();
		vec
	}
}