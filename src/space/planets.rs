pub struct Planet {
	name: String,

	texture: char,
	color: Color,
	radius: u8,
}

// Things that should go to their own files. Borrador.
pub struct Star {
	name: Option<String>,

	color: Color,
}

pub struct BlackHole {
	name: String,
}

pub struct SpaceShip {
	name: String,
	icon: char,
	color: Color,

	price: u16,

	energy: u8,
	OR
	power_cell: Option<PowerCell>,
	modules: HashSet<Module>,
}