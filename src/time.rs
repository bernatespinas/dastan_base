#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Season {
	Winter,
	Spring,
	Summer,
	Autumn,
}

impl Season {
	pub fn next(season: Season) -> Season {
		match season {
			Season::Winter	=> Season::Spring,
			Season::Spring	=> Season::Summer,
			Season::Summer	=> Season::Autumn,
			Season::Autumn	=> Season::Winter,
		}
	}

	pub fn from(month: Month) -> Season {
		match month {
			Month::December		=> Season::Winter,
			Month::January		=> Season::Winter,
			Month::February		=> Season::Winter,
			Month::March		=> Season::Spring,
			Month::April 		=> Season::Spring,
			Month::May			=> Season::Spring,
			Month::June			=> Season::Summer,
			Month::July			=> Season::Summer,
			Month::August		=> Season::Summer,
			Month::September	=> Season::Autumn,
			Month::October		=> Season::Autumn,
			Month::November		=> Season::Autumn,
		}
	}

	pub fn part_of_day(self, hour: u8) -> PartOfDay {
		match self {
			Season::Winter => {
				if hour >= 5 && hour <= 11 {
					PartOfDay::Morning
				}
				else if hour >= 12 && hour <= 16 {
					PartOfDay::Afternoon
				}
				else if hour >= 17 && hour <= 20 {
					PartOfDay::Evening
				}
				else {
				// else if self.hour >= 21 && self.hour <= 4 {
					PartOfDay::Night
				}
			},
			Season::Spring => {
				if hour >= 5 && hour <= 11 {
					PartOfDay::Morning
				}
				else if hour >= 12 && hour <= 16 {
					PartOfDay::Afternoon
				}
				else if hour >= 17 && hour <= 20 {
					PartOfDay::Evening
				}
				else {
				// else if self.hour >= 21 && self.hour <= 4 {
					PartOfDay::Night
				}
			},
			Season::Summer => {
				if hour >= 5 && hour <= 11 {
					PartOfDay::Morning
				}
				else if hour >= 12 && hour <= 16 {
					PartOfDay::Afternoon
				}
				else if hour >= 17 && hour <= 20 {
					PartOfDay::Evening
				}
				else {
				// else if self.hour >= 21 && self.hour <= 4 {
					PartOfDay::Night
				}
			},
			Season::Autumn => {
				if hour >= 5 && hour <= 11 {
					PartOfDay::Morning
				}
				else if hour >= 12 && hour <= 16 {
					PartOfDay::Afternoon
				}
				else if hour >= 17 && hour <= 20 {
					PartOfDay::Evening
				}
				else {
				// else if self.hour >= 21 && self.hour <= 4 {
					PartOfDay::Night
				}
			},
		}
	}
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Month {
	January,
	February,
	March,
	April,
	May,
	June,
	July,
	August,
	September,
	October,
	November,
	December,
}

impl Month {
	pub fn next(month: Month) -> Month {
		match month {
			Month::January		=> Month::February,
			Month::February		=> Month::March,
			Month::March		=> Month::April,
			Month::April		=> Month::May,
			Month::May			=> Month::June,
			Month::June			=> Month::July,
			Month::July			=> Month::August,
			Month::August		=> Month::September,
			Month::September	=> Month::October,
			Month::October		=> Month::November,
			Month::November		=> Month::December,
			Month::December		=> Month::January,
		}
	}
	pub fn get_season(self) -> Season {
		match self {
			Month::December		=> Season::Winter,
			Month::January		=> Season::Winter,
			Month::February		=> Season::Winter,
			Month::March		=> Season::Spring,
			Month::April 		=> Season::Spring,
			Month::May			=> Season::Spring,
			Month::June			=> Season::Summer,
			Month::July			=> Season::Summer,
			Month::August		=> Season::Summer,
			Month::September	=> Season::Autumn,
			Month::October		=> Season::Autumn,
			Month::November		=> Season::Autumn,
		}
	}
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum WeekDay {
	Monday,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Saturday,
	Sunday,
}

impl WeekDay {
	pub fn next(week_day: &WeekDay) -> WeekDay {
		match week_day {
			WeekDay::Monday		=> WeekDay::Tuesday,
			WeekDay::Tuesday 	=> WeekDay::Wednesday,
			WeekDay::Wednesday	=> WeekDay::Thursday,
			WeekDay::Thursday	=> WeekDay::Friday,
			WeekDay::Friday		=> WeekDay::Saturday,
			WeekDay::Saturday	=> WeekDay::Sunday,
			WeekDay::Sunday		=> WeekDay::Monday,
		}
	}
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum PartOfDay {
	Morning,
	Afternoon,
	Evening,
	Night,
}

// impl PartOfDay {
// 	pub fn from(hour: u8, season: Season) {
//
// 	}
// }

#[derive(Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Hour {
	hour: u8,
	minute: u8,
}

impl std::fmt::Display for Hour {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
		if self.minute < 10 {
			write!(f, "{}:0{}", self.hour, self.minute)
		}
		else {
			write!(f, "{}:{}", self.hour, self.minute)
		}
	}
}

impl Default for Hour {
	fn default() -> Hour {
		Hour {
			hour: 0,
			minute: 0,
		}
	}
}

// TODO Return the max amount of time that has passed? A day when hour goes back to 0, an hour when minute goes back to 0 AND hour does not...
impl Hour {
	// pub fn new(hour: u8, minute: u8) -> Result<Hour, String> {
	// 	if hour <= 23 && minute <= 59 {
	// 		Ok(
	// 			Hour {
	// 				hour,
	// 				minute,
	// 			}
	// 		)
	// 	}
	// 	else {
	// 		Err("hour > 23 || minute > 59".to_string())
	// 	}
	// }

	pub fn hour(&self) -> u8 {
		self.hour
	}

	pub fn minute(&self) -> u8 {
		self.minute
	}

	pub fn next_minute(&mut self) {
		if self.minute < 59 {
			self.minute += 1;
		}
		else {
			self.minute = 0;
			self.next_hour();
		}
	}

	pub fn next_hour(&mut self) {
		if self.hour < 23 {
			self.hour += 1;
		}
		else {
			self.hour = 0;
		}
	}
}

#[derive(Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Day {
	week_day: WeekDay,
	number: u8,
}

impl Default for Day {
	fn default() -> Day {
		Day {
			week_day: WeekDay::Monday,
			number: 1,
		}
	}
}

impl Day {
	// pub fn new(week_day: WeekDay, number: u8) -> Result<Day, ()> {
	// 	if number > 28 || number == 0 {
	// 		Err(())
	// 	}
	// 	else {
	// 		Ok(
	// 			Day {
	// 				week_day,
	// 				number,
	// 			}
	// 		)
	// 	}
	// }

	pub fn week_day(&self) -> WeekDay {
		self.week_day
	}

	pub fn number(&self) -> u8 {
		self.number
	}

	pub fn next(&mut self) {
		self.week_day = WeekDay::next(&self.week_day);
		self.number = {
			if self.number == 28 {1}
			else {self.number+1}
		}
	}
}