use std::collections::hash_map::Iter;

mod organic_body;
pub use organic_body::{OrganicBody, OrganicSlot};

mod spaceship_body;
pub use spaceship_body::{SpaceshipBody, SpaceshipSlot, Module};

pub trait Body {
	type Slot;
	type Piece;

	fn get(&self, slot: Self::Slot) -> Option<&Option<Self::Piece>>;
	fn can_equip_in(&self, slot: Self::Slot) -> bool;
	fn has_slot(&self, slot: Self::Slot) -> bool;
	fn slot_is_free(&self, slot: Self::Slot) -> bool;

	// fn add_slot(&mut self, slot: Self::Slot);
	// fn del_slot(&mut self, slot: Self::Slot);
	
	fn equip(&mut self, piece: Self::Piece);
	fn dequip(&mut self, slot: Self::Slot) -> Option<Self::Piece>;
	
	fn add_attributes(&mut self, piece: &Self::Piece);
	fn del_attributes(&mut self, piece: &Self::Piece);

	fn iter_equipment(&self) -> Iter<Self::Slot, Option<Self::Piece>>;
}