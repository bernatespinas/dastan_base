use std::collections::HashMap;
use std::collections::hash_map::Iter;

use uuid::Uuid;

use crate::{
	Color, Size, Weight,
	Body,
	Device, DeviceId,
	Generator,
};
use crate::creature::body::{SpaceshipBody, SpaceshipSlot, Module};
use super::{Creature, ToCreature};

const BROKEN_SPACESHIP: char = '*';
const SPACESHIP: char = '>';

#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Serialize, Deserialize)]
pub enum SpaceshipCategory {
	Scout,
	Merchant,
	Traveler,
	More,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Spaceship {
	name: String,
	icon: char,
	color: Color,

	category: SpaceshipCategory,

	hp: u16,

	energy: u16,
	energy_consumption_per_turn: u8,

	body: SpaceshipBody,

	device_id: Option<Uuid>,

	/// The DeviceId of the Passage the player will be teleported to when entering the Spaceship.
	pub teleporter_id: Option<DeviceId>,
}

impl ToCreature for Spaceship {
	fn to_creature(self) -> Creature {
		Creature::Spaceship(Box::new(self))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn color(&self) -> Color {
		self.color
	}

	fn size(&self) -> Size {
		Size::Huge
	}

	fn weight(&self) -> Weight {
		Weight::SuperHeavy
	}

	fn vitality(&self) -> u16 {
		self.body.vitality
	}

	fn strength(&self) -> u16 {
		self.body.strength
	}

	fn defense(&self) -> u16 {
		self.body.defense
	}
	
	fn is_alive(&self) -> bool {
		self.hp > 0
	}

	fn take_damage(&mut self, damage: u16) -> String {
		if damage < self.hp {
			self.hp -= damage;
			return format!("{} takes {} damage.",
							self.name, damage
			);
		}
		else {
			self.die();

			return format!("{} takes {} damage is/gets destroyed!",
							self.name, damage
			);
		}
	}

	fn die(&mut self) -> String {
		self.hp = 0;
		self.color = Color::Red;
		self.icon = BROKEN_SPACESHIP;

		format!("{} is destroyed!", self.name)
	}

	fn hp(&self) -> u16 {
		self.hp
	}
}

impl Device for Spaceship {
	fn device_id(&self) -> Option<DeviceId> {
		self.device_id.map(|uuid| DeviceId::Spaceship(uuid))
	}

	fn set_random_device_id(mut self) -> Spaceship {
		self.device_id = Some(Uuid::new_v4());
		self
	}
}

impl Body for Spaceship {
	type Slot = SpaceshipSlot;
	type Piece = Module;

	fn get(&self, slot: Self::Slot) -> Option<&Option<Self::Piece>> {
		self.body.get(slot)
	}
	
	fn can_equip_in(&self, slot: Self::Slot) -> bool {
		self.body.can_equip_in(slot)
	}
	
	fn has_slot(&self, slot: Self::Slot) -> bool {
		self.body.has_slot(slot)
	}
	
	fn slot_is_free(&self, slot: Self::Slot) -> bool {
		self.body.slot_is_free(slot)
	}
	
	fn equip(&mut self, piece: Self::Piece) {
		self.body.equip(piece)
	}
	
	fn dequip(&mut self, slot: Self::Slot) -> Option<Self::Piece> {
		self.body.dequip(slot)
	}
	
	fn add_attributes(&mut self, piece: &Self::Piece) {
		self.body.add_attributes(piece)
	}
	
	fn del_attributes(&mut self, piece: &Self::Piece) {
		self.body.del_attributes(piece)
	}
	
	fn iter_equipment(&self) -> Iter<Self::Slot, Option<Self::Piece>> {
		self.body.iter_equipment()
	}
}

pub struct SpaceshipGen<'a> {
	hashmap: HashMap<&'a str, Spaceship>,
}

impl<'a> SpaceshipGen<'a> {
	pub fn generate() -> SpaceshipGen<'a> {
		let mut hashmap: HashMap<&'a str, Spaceship> = HashMap::new();

		hashmap.insert("MySporeCreation1", Spaceship {
			name: "MySporeCreation1".to_string(),
			icon: SPACESHIP,
			color: Color::LGrey,

			category: SpaceshipCategory::Scout,

			hp: 220,

			energy: 1250,
			energy_consumption_per_turn: 2,

			body: SpaceshipBody::new(15, 15, 15)
				.with_slots(vec!(
					SpaceshipSlot::Hull,
					SpaceshipSlot::Shield,
					SpaceshipSlot::Weapon,
				))
			,

			device_id: None,
			
			teleporter_id: None,
		});

		SpaceshipGen {
			hashmap
		}
	}
}

impl<'a> Generator for SpaceshipGen<'a> {
	type Output = Spaceship;

	fn contains(&self, code: &str) -> bool {
		self.hashmap.contains_key(code)
	}

	fn code(&self, code: &str) -> Result<String, String> {
		if self.hashmap.contains_key(code) {
			Ok(code.to_string())
		}
		else {
			Err(format!("Code {} not valid in SpaceshipGen", code))
		}
	}

	fn get(&self, code: &str) -> Spaceship {
		self.hashmap.get(code)
			.unwrap_or_else(|| panic!("Code {} not valid in SpaceshipGen", code))
			.clone()
			.set_random_device_id()
	}

	fn list(&self) -> Vec<&str> {
		let mut vec: Vec<&str> = Vec::with_capacity(self.hashmap.len());
		for key in self.hashmap.keys() {
			vec.push(key);
		}
		vec.sort();
		vec
	}
}