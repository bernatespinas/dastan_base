use std::collections::HashMap;
use std::collections::hash_map::Iter;

use crate::{Body, OrganicBody, OrganicSlot, Creature, ToCreature, Item, Equipment, Generator, Color, Size, Weight};

pub const COW: char = 'À';
pub const HORSE: char = 'Á';

#[derive(Clone, Serialize, Deserialize)]
pub struct Animal {
	name: String,
	icon: char,
	pub color: Color,

	blocks_sight: bool,
	size: Size,
	weight: Weight,

	hp: u16,

	body: OrganicBody,

	// hunger: u32,
	// hunger_rate: u8,

	// fov_radius: u8,
	// hearing_radius: u8,

	// pub attack_mode: bool,

	drops: Vec<Item>,
}

impl ToCreature for Animal {
	fn to_creature(self) -> Creature {
		Creature::Animal(Box::new(self))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn color(&self) -> Color {
		self.color
	}

	fn size(&self) -> Size {
		self.size
	}

	fn weight(&self) -> Weight {
		self.weight
	}

	fn vitality(&self) -> u16 {
		self.body.vitality
	}

	fn strength(&self) -> u16 {
		self.body.strength
	}

	fn defense(&self) -> u16 {
		self.body.defense
	}

	fn is_alive(&self) -> bool {
		self.hp > 0
	}

	fn take_damage(&mut self, damage: u16) -> String {
		if damage < self.hp {
			self.hp -= damage;
			return format!("{} takes {} damage.",
						self.name, damage);
		}
		else {
			self.die();
			return format!("{} takes {} damage and dies!",
						self.name, damage);
		}
	}

	fn die(&mut self) -> String {
		self.hp = 0;
		self.color = Color::Red;

		format!("{} dies!", self.name)
	}

	fn hp(&self) -> u16 {
		self.hp
	}
}

impl Body for Animal {
	type Slot = OrganicSlot;
	type Piece = Equipment;

	fn get(&self, slot: Self::Slot) -> Option<&Option<Self::Piece>> {
		self.body.get(slot)
	}
	
	fn can_equip_in(&self, slot: Self::Slot) -> bool {
		self.body.can_equip_in(slot)
	}
	
	fn has_slot(&self, slot: Self::Slot) -> bool {
		self.body.has_slot(slot)
	}
	
	fn slot_is_free(&self, slot: Self::Slot) -> bool {
		self.body.slot_is_free(slot)
	}
	
	fn equip(&mut self, piece: Self::Piece) {
		self.body.equip(piece)
	}
	
	fn dequip(&mut self, slot: Self::Slot) -> Option<Self::Piece> {
		self.body.dequip(slot)
	}
	
	fn add_attributes(&mut self, piece: &Self::Piece) {
		self.body.add_attributes(piece)
	}
	
	fn del_attributes(&mut self, piece: &Self::Piece) {
		self.body.del_attributes(piece)
	}
	
	fn iter_equipment(&self) -> Iter<Self::Slot, Option<Self::Piece>> {
		self.body.iter_equipment()
	}
}

pub struct AnimalGen<'a> {
	hashmap: HashMap<&'a str, Animal>,
}

impl<'a> AnimalGen<'a> {
	pub fn generate() -> AnimalGen<'a> {
		let mut hashmap: HashMap<&str, Animal> = HashMap::new();

		hashmap.insert("COW", Animal {
			name: "Cow".to_string(),
			icon: COW,
			color: Color::DGrey,

			blocks_sight: true,
			size: Size::Big,
			weight: Weight::Heavy,

			hp: 85,

			body: OrganicBody::new(8, 7, 4),

			drops: Vec::new(),
		});

		hashmap.insert("HORSE", Animal {
			name: "Horse".to_string(),
			icon: HORSE,
			color: Color::DBrown,

			blocks_sight: true,
			size: Size::Big,
			weight: Weight::Heavy,

			hp: 95,

			body: OrganicBody::new(9, 6, 5),

			drops: Vec::new(),
		});

		AnimalGen {
			hashmap,
		}
	}
}

impl<'a> Generator for AnimalGen<'a> {
	type Output = Animal;
	
	fn contains(&self, code: &str) -> bool {
		self.hashmap.contains_key(code)
	}

	fn code(&self, code: &str) -> Result<String, String> {
		if self.hashmap.contains_key(code) {
			Ok(code.to_string())
		}
		else {
			Err(format!("Code {} not valid in AnimalGen", code))
		}
	}

	fn get(&self, code: &str) -> Animal {
		self.hashmap.get(code)
			.unwrap_or_else(|| panic!("Code {} not valid in AnimalGen", code))
			.clone()
	}

	fn list(&self) -> Vec<&str> {
		let mut vec: Vec<&str> = Vec::with_capacity(self.hashmap.len());
		for key in self.hashmap.keys() {
			vec.push(key);
		}
		vec.sort();
		vec
	}
}