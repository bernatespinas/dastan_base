use std::collections::HashMap;
use std::collections::hash_map::Iter;
use std::fmt;

use crate::ListEnum;
use crate::item::equipment::Equipment;

use super::Body;


#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Serialize, Deserialize)]
pub enum OrganicSlot {
	Head,
	Torso,
	Hands,
	Legs,
	Feet,

	Fingers,
	Neck,
	Wrists,
	Waist,

	Weapon,
}

impl fmt::Display for OrganicSlot {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), fmt::Error> {
		write!(f, "{:?}", self)
	}
}

impl ListEnum for OrganicSlot {
	fn list() -> Vec<OrganicSlot> {
		vec!(
			OrganicSlot::Head,
			OrganicSlot::Torso,
			OrganicSlot::Hands,
			OrganicSlot::Legs,
			OrganicSlot::Feet,

			OrganicSlot::Fingers,
			OrganicSlot::Neck,
			OrganicSlot::Wrists,
			OrganicSlot::Waist,

			OrganicSlot::Weapon
		)
	}
}

// Vitality, strength and defense would be relevant for any type of Body.
#[derive(Clone, Serialize, Deserialize)]
pub struct OrganicBody {
	pub vitality: u16,
	pub strength: u16,
	pub defense: u16,
	pieces: HashMap<OrganicSlot, Option<Equipment>>,
}

impl OrganicBody {
	pub fn new(vitality: u16, strength: u16, defense: u16) -> OrganicBody {
		OrganicBody {
			vitality,
			strength,
			defense,
			pieces: HashMap::new(),
		}
	}

	pub fn with_slots(mut self, slots: Vec<OrganicSlot>) -> OrganicBody {
		self.pieces = HashMap::with_capacity(slots.len());

		for slot in slots {
			self.pieces.insert(slot, None);
		}

		self
	}

	pub fn with_pieces(mut self, pieces: Vec<(OrganicSlot, Equipment)>) -> OrganicBody {
		for (slot, piece) in pieces {
			self.pieces.insert(slot, Some(piece));
		}

		self
	}
}

impl Body for OrganicBody {
	type Slot = OrganicSlot;
	type Piece = Equipment;

	fn get(&self, slot: OrganicSlot) -> Option<&Option<Equipment>> {
		self.pieces.get(&slot)
	}

	fn can_equip_in(&self, slot: OrganicSlot) -> bool {
		self.has_slot(slot) && self.slot_is_free(slot)
	}

	fn has_slot(&self, slot: OrganicSlot) -> bool {
		self.pieces.contains_key(&slot)
	}

	// TODO: Use has_slot inside? If the slot isn't present, it returns true...
	fn slot_is_free(&self, slot: OrganicSlot) -> bool {
		if let Some(Some(_equipment)) = self.pieces.get(&slot) {
			false
		} else {
			true
		}
	}

	fn equip(&mut self, equipment: Equipment) {
		self.add_attributes(&equipment);
		self.pieces.insert(equipment.slot, Some(equipment));
	}

	fn dequip(&mut self, slot: OrganicSlot) -> Option<Equipment> {
		let piece = self.pieces.insert(slot, None).unwrap();
		
		if let Some(equipment) = &piece {
			self.del_attributes(equipment);
		}

		piece
	}

	fn add_attributes(&mut self, equipment: &Equipment) {
		self.vitality += equipment.vitality;
		self.strength += equipment.strength;
		self.defense += equipment.defense;
	}

	fn del_attributes(&mut self, equipment: &Equipment) {
		self.vitality -= equipment.vitality;
		self.strength -= equipment.strength;
		self.defense -= equipment.defense;
	}

	fn iter_equipment(&self) -> Iter<OrganicSlot, Option<Equipment>> {
		self.pieces.iter()
	}
}