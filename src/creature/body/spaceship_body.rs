use std::collections::HashMap;
use std::collections::hash_map::Iter;
use std::fmt;

use crate::ListEnum;

use super::Body;

#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Serialize, Deserialize)]
pub enum SpaceshipSlot {
	Weapon,
	Hull,
	Shield,
}

impl fmt::Display for SpaceshipSlot {
	fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
		write!(f, "{:?}", self)
	}
}

impl ListEnum for SpaceshipSlot {
	fn list() -> Vec<SpaceshipSlot> {
		vec!(
			SpaceshipSlot::Weapon,
			SpaceshipSlot::Hull,
			SpaceshipSlot::Shield
		)
	}
}

#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Serialize, Deserialize)]
pub struct Module {
	slot: SpaceshipSlot,

	vitality: u16,
	strength: u16,
	defense: u16,

	speed: i8,
	fuel: i8,
	inventory: i8,
	energy: i8,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct SpaceshipBody {
	pub vitality: u16,
	pub strength: u16,
	pub defense: u16,

	pieces: HashMap<SpaceshipSlot, Option<Module>>,
}

impl SpaceshipBody {
	pub fn new(vitality: u16, strength: u16, defense: u16) -> Self {
		SpaceshipBody {
			vitality,
			strength,
			defense,

			pieces: HashMap::new(),
		}
	}

	pub fn with_slots(mut self, slots: Vec<SpaceshipSlot>) -> SpaceshipBody {
		self.pieces = HashMap::with_capacity(slots.len());

		for slot in slots {
			self.pieces.insert(slot, None);
		}

		self
	}

	pub fn with_pieces(mut self, pieces: Vec<(SpaceshipSlot, Module)>) -> SpaceshipBody {
		for (slot, piece) in pieces {
			self.pieces.insert(slot, Some(piece));
		}

		self
	}
}

impl Body for SpaceshipBody {
	type Slot = SpaceshipSlot;
	type Piece = Module;

	fn get(&self, slot: Self::Slot) -> Option<&Option<Self::Piece>> {
		self.pieces.get(&slot)
	}

	fn can_equip_in(&self, slot: Self::Slot) -> bool {
		self.has_slot(slot) && self.slot_is_free(slot)
	}

	fn has_slot(&self, slot: Self::Slot) -> bool {
		self.pieces.contains_key(&slot)
	}

	fn slot_is_free(&self, slot: Self::Slot) -> bool {
		if let Some(Some(_)) = self.pieces.get(&slot) {
			false
		} else {
			true
		}
	}

	fn equip(&mut self, piece: Self::Piece) {
		self.add_attributes(&piece);
		self.pieces.insert(piece.slot, Some(piece));
	}

	fn dequip(&mut self, slot: Self::Slot) -> Option<Self::Piece> {
		let piece = self.pieces.insert(slot, None).unwrap();

		if let Some(piece) = &piece {
			self.del_attributes(piece);
		}

		piece
	}

	fn add_attributes(&mut self, piece: &Self::Piece) {
		self.vitality += piece.vitality;
		self.strength += piece.strength;
		self.defense += piece.defense;
	}

	fn del_attributes(&mut self, piece: &Self::Piece) {
		self.vitality -= piece.vitality;
		self.strength -= piece.strength;
		self.defense -= piece.defense;
	}

	fn iter_equipment(&self) -> Iter<Self::Slot, Option<Self::Piece>> {
		self.pieces.iter()
	}
}