use std::collections::HashMap;
use std::collections::hash_map::Iter;

use crate::{
	Creature, ToCreature, Body, OrganicBody, Inventory, QuestLog,
	Item, ToItem,
	Equipment,
	Food,
	RecipeBook, SpellBook,
	Generator, ItemBundle, RecipeGen,
	ListEnum, Effect, OrganicSlot,
	Color, Size, Weight,
};

#[derive(Debug, PartialEq, Copy, Clone, Serialize, Deserialize)]
pub enum Faction {
	Good,
	Evil,
}

impl ListEnum for Faction {
	fn list() -> Vec<Faction> {
		vec!(
			Faction::Good,
			Faction::Evil,
		)
	}
}

#[derive(Debug, PartialEq, Copy, Clone, Serialize, Deserialize)]
pub enum Race {
	Human,
	Elf,
	Dwarf,
	Gnome,
	Undead,
	Orc,
	Troll,
	Goblin,
}

impl std::fmt::Display for Race {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
		write!(f, "{:?}", self)
	}
}

impl ListEnum for Race {
	fn list() -> Vec<Race> {
		vec!(
			Race::Human,
			Race::Elf,
			Race::Dwarf,
			Race::Gnome,
			Race::Undead,
			Race::Orc,
			Race::Troll,
			Race::Goblin,
		)
	}
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum Mood {
	Happy,
	Angry,
	Sad,
	Neutral,
}

// TODO
// pub struct SocialTODO {
// 	mood: Mood,
// 	friends: Vec<String>
// }

// impl SocialTODO {
// 	fn new() -> SocialTODO {
// 		SocialTODO {
// 			mood: Mood::Neutral,
// 			friends: Vec::new(),
// 		}
// 	}
// }

#[derive(Clone, Serialize, Deserialize)]
pub struct Humanoid {
	pub name: String,
	pub icon: char,
	pub color: Color,

	blocks_sight: bool,
	size: Size,
	weight: Weight,

	pub hp: u16,
	pub sp: u16,

	// faction: seeds::Faction,
	// race: seeds::Race,

	body: OrganicBody,

	hunger: u16,
	hunger_rate: u8,

	fov_radius: u8,
	hearing_radius: u8,

	attack_mode: bool,

	spell_book: SpellBook,

	drops: Vec<Item>,

	// Crafter.
	pub inventory: Inventory,
	pub recipe_book: RecipeBook,

	// Social.
	mood: Mood,
	// reputation: u16,
	pub quests: QuestLog,
	// friends: Vec<String>,
}

impl Humanoid {
	fn heal(&mut self, amount: u16) -> String {
		self.hp += amount;
		if self.hp > self.max_hp() {
			self.hp = self.max_hp();
		}

		format!("{} heals for {} hp.",
				self.name, amount)
	}

	fn feed(&mut self, amount: u16) -> String {
		if amount <= self.hunger {
			self.hunger -= amount;
		}
		else {
			self.hunger = 0;
		}

		// if self.hunger < -self.hunger/4 {
		// 	self.puke_ewwwWWWW();
		// }

		format!("{} -{} feed points less hunger YIM YUM!!",
			self.name, amount)
	}

	// // TODO: Ummmmm......... Make it return an Option<String> or Option<&str>. If it's None, it can't be used; if it's Some(VERB), it can be used and the game will use the VERB (eat, drink, equip...).
	// fn can_use(&self, item: &Item) -> bool {
	// 	match item.component {
	// 		ItemComponent::Food{..}		=> true,
	// 		ItemComponent::Equipment{slot, ..} =>
	// 			self.equipment.can_equip_in(&slot),
	// 		ItemComponent::Corpse{..}	=> true,
	// 		ItemComponent::None			=> false,
	// 	}
	// }

	// fn use(&mut self, item: &Item) {
	// 	match item.component {
	// 		ItemComponent::Food{..}			=> self.eat(???),
	// 		ItemComponent::Equipment{..}	=> self.equip(???),
	// 		ItemComponent::Corpse{..}		=> self.loot(???),
	// 		ItemComponent::None				=> (),
	// 	};
	// }
	//
	// fn use(&mut self, item: Item) {
	// 	match item.component {
	// 		ItemComponent::Food{effect, amount} => match effect {
	// 			Effect::Feed	=> self.feed(amount),
	// 			Effect::Heal	=> self.heal(amount),
	// 			Effect::None	=> (),
	// 		},
	// 		ItemComponent::Equipment{..}	=> self.equip(item),
	// 		ItemComponent::Corpse{..}		=> self.loot(item)
	// 	}
	// }

	pub fn eat(&mut self, food: Food) {
		match food.effect {
			Effect::Heal(amount)	=> {self.heal(amount);},
			Effect::Feed(amount)	=> {self.feed(amount);},
			Effect::None			=> (),
		}
	}

	pub fn max_hp(&self) -> u16 {
		self.body.vitality * 10
	}
}

impl ToCreature for Humanoid {
	fn to_creature(self) -> Creature {
		Creature::Humanoid(Box::new(self))
	}

	fn name(&self) -> &str {
		&self.name
	}

	fn icon(&self) -> char {
		self.icon
	}

	fn color(&self) -> Color {
		self.color
	}

	fn size(&self) -> Size {
		self.size
	}

	fn weight(&self) -> Weight {
		self.weight
	}

	fn vitality(&self) -> u16 {
		self.body.vitality
	}

	fn strength(&self) -> u16 {
		self.body.strength
	}

	fn defense(&self) -> u16 {
		self.body.defense
	}
	
	fn is_alive(&self) -> bool {
		self.hp > 0
	}

	fn take_damage(&mut self, damage: u16) -> String {
		if damage < self.hp {
			self.hp -= damage;
			return format!("{} takes {} damage.",
						self.name, damage);
		}
		else {
			self.die();
			return format!("{} takes {} damage and dies!",
						self.name, damage);
		}
	}

	fn die(&mut self) -> String {
		self.hp = 0;
		self.color = Color::Red;

		format!("{} dies!", self.name)
	}

	fn hp(&self) -> u16 {
		self.hp
	}
}

impl Body for Humanoid {
	type Slot = OrganicSlot;
	type Piece = Equipment;

	fn get(&self, slot: Self::Slot) -> Option<&Option<Self::Piece>> {
		self.body.get(slot)
	}
	
	fn can_equip_in(&self, slot: Self::Slot) -> bool {
		self.body.can_equip_in(slot)
	}
	
	fn has_slot(&self, slot: Self::Slot) -> bool {
		self.body.has_slot(slot)
	}
	
	fn slot_is_free(&self, slot: Self::Slot) -> bool {
		self.body.slot_is_free(slot)
	}
	
	fn equip(&mut self, piece: Self::Piece) {
		self.body.equip(piece)
	}
	
	fn dequip(&mut self, slot: Self::Slot) -> Option<Self::Piece> {
		self.body.dequip(slot)
	}
	
	fn add_attributes(&mut self, piece: &Self::Piece) {
		self.body.add_attributes(piece)
	}
	
	fn del_attributes(&mut self, piece: &Self::Piece) {
		self.body.del_attributes(piece)
	}
	
	fn iter_equipment(&self) -> Iter<Self::Slot, Option<Self::Piece>> {
		self.body.iter_equipment()
	}
}

pub struct HumanoidGen<'a> {
	hashmap: HashMap<&'a str, Humanoid>,
}

impl<'a> HumanoidGen<'a> {
	pub fn generate_player(
		name: &str,
		item_gen_bundle: &ItemBundle,
		recipe_gen: &RecipeGen
	) -> Humanoid {

		Humanoid {
			name: name.to_string(),
			icon: '@',
			color: Color::Yellow,

			blocks_sight: true,
			size: Size::Medium,
			weight: Weight::Medium,

			body: OrganicBody::new(1, 1, 1)
				.with_slots(vec!(
					OrganicSlot::Head,
					OrganicSlot::Torso,
					OrganicSlot::Hands,
					OrganicSlot::Legs,
					OrganicSlot::Feet,
					OrganicSlot::Weapon,
				))
			,

			hp: 10,
			sp: 0,

			hunger: 100,
			hunger_rate: 1,

			fov_radius: 30,
			hearing_radius: 20,

			attack_mode: false,

			drops: Vec::new(),

			spell_book: SpellBook::default(),

			// Crafter.
			inventory: Inventory::new(10, vec!(
				item_gen_bundle.equipment_gen.get("SWORD_IRON").to_item(),
				item_gen_bundle.equipment_gen.get("HELMET_LEATHER").to_item(),
			)),

			recipe_book: RecipeBook::default().with_recipes(
				vec!(recipe_gen.get("Stone wall blueprint"))
			),

			//Social.
			mood: Mood::Neutral,
			quests: QuestLog::default(),
		}
	}

	pub fn generate(item_gen_bundle: &ItemBundle) -> HumanoidGen<'a> {

		let mut hashmap: HashMap<&str, Humanoid> = HashMap::new();

		hashmap.insert("ORC", Humanoid {
			name: "Orc".to_string(),
			icon: 'o',
			color: Color::LGreen,

			blocks_sight: true,
			size: Size::Medium,
			weight: Weight::Medium,

			body: OrganicBody::new(1, 0, 0)
				.with_slots(vec!(OrganicSlot::Head))
			,

			hp: 10,
			sp: 0,

			hunger: 100,
			hunger_rate: 1,

			fov_radius: 20,
			hearing_radius: 10,

			attack_mode: false,

			drops: Vec::new(),

			spell_book: SpellBook::default(),

			// Crafter.
			inventory: Inventory::new(5, vec!(
				item_gen_bundle.food_gen.get("POTION_HEALING").to_item(),
			)),

			recipe_book: RecipeBook::default(),

			// Social.
			mood: Mood::Neutral,
			quests: QuestLog::default(),
		});

		hashmap.insert("TROLL", Humanoid {
			name: "Troll".to_string(),
			icon: 'T',
			color: Color::DGreen,

			blocks_sight: true,
			size: Size::Medium,
			weight: Weight::Medium,

			body: OrganicBody::new(3, 4, 1)
				.with_slots(vec!(OrganicSlot::Head))
			,

			hp: 30,
			sp: 0,

			hunger: 100,
			hunger_rate: 1,

			fov_radius: 20,
			hearing_radius: 10,

			attack_mode: false,

			drops: Vec::new(),

			spell_book: SpellBook::default(),

			// Crafter.
			inventory: Inventory::new(5, vec!(
				item_gen_bundle.food_gen.get("POTION_HEALING").to_item(),
				item_gen_bundle.food_gen.get("POTION_FEEDING").to_item(),
			)),

			recipe_book: RecipeBook::default(),

			// Social
			mood: Mood::Neutral,
			quests: QuestLog::default(),
		});

		hashmap.insert("ELF", Humanoid {
			name: "Elf".to_string(),
			icon: 'f',
			color: Color::LBlue,

			blocks_sight: true,
			size: Size::Medium,
			weight: Weight::Medium,

			hp: 50,
			sp: 0,

			body: OrganicBody::new(5, 3, 2)
				.with_slots(vec!(OrganicSlot::Head))
			,

			hunger: 100,
			hunger_rate: 1,

			fov_radius: 20,
			hearing_radius: 10,

			attack_mode: false,

			drops: Vec::new(),

			spell_book: SpellBook::default(),

			// Crafter.
			inventory: Inventory::new(5, vec!(
				item_gen_bundle.food_gen.get("POTION_HEALING").to_item(),
				item_gen_bundle.food_gen.get("POTION_HEALING").to_item(),
				item_gen_bundle.food_gen.get("POTION_HEALING").to_item(),
			)),

			recipe_book: RecipeBook::default(),

			// Social.
			mood: Mood::Neutral,
			quests: QuestLog::default(),
		});

		HumanoidGen {
			hashmap
		}
	}
}

impl<'a> Generator for HumanoidGen<'a> {
	type Output = Humanoid;

	fn contains(&self, code: &str) -> bool {
		self.hashmap.contains_key(code)
	}

	fn code(&self, code: &str) -> Result<String, String> {
		if self.hashmap.contains_key(code) {
			Ok(code.to_string())
		}
		else {
			Err(format!("Code {} not valid in HumanoidGen", code))
		}
	}

	fn get(&self, code: &str) -> Humanoid {
		self.hashmap.get(code)
			.unwrap_or_else(|| panic!("Code {} not valid in HumanoidGen", code))
			.clone()
	}

	fn list(&self) -> Vec<&str> {
		let mut vec: Vec<&str> = Vec::with_capacity(self.hashmap.len());
		for key in self.hashmap.keys() {
			vec.push(key);
		}
		vec.sort();
		vec
	}
}