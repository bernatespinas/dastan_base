struct ElierozIconPack {}

impl IconPack fro ElierozIconPack {
	fn TODO(&self, icon: Icon) -> char {
		match icon {
			Player	=> '@',
			Orc		=> 'o',
			Troll	=> 'T',
			Elf		=> 'f',
		}
	}
}