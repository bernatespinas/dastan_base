mod item;
use item::{Item, Food, ItemComponent};

// Things that should be 'use'd, but I just want to test the "idea".
pub struct Color {

}

pub struct EntityMaybeOrSomething<T: Component> {
	name: String,
	icon: char,
	color: Color,

	// Some thingies always or never block passage, so it shouldn't be stored in every of them.
	// blocks_passage: bool,

	component: T,
}

// impl ??? for EntityMaybeOrSomething<Item huh Equipment> {
// 	fn name(&self) -> String {
// 		format!("{} {}", self.modifier.as_string(), self.name)
// 	}
// }

// for Food {
// 	rotten + self.name
// }

pub trait Component {
	fn blocks_passage(&self) -> bool;
}

// enum Component {
// 	Creature(Creature),
// 	Item(Item),
// }

struct Tile {

}

pub struct Inventory {
	// Qualsevol tipus d'Item.
	// Em diu que ItemComponent no està implementat per a &'a dyn ItemComponent + 'a... Okay... -_- A més, tinc una lifetime, ugh.
	// items: Vec<EntityMaybeOrSomething<Item<&'a dyn ItemComponent>>>,
	items: Vec<EntityMaybeOrSomething<Item<Box<ItemComponent>>>>,

	// Només Food.
	foods: Vec<EntityMaybeOrSomething<Item<Food>>>,
}

// Huh, having a "general" struct that can be a Creature or an Item or a Tile... Maybe it's too "general"? Maybe less-general-but-still structs Creature, Item and Tile would be better? With their components.