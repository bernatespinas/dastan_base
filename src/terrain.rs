use crate::{Color, Generator};

mod foundation;
pub use foundation::{Foundation, FoundationGen, FoundationType};

mod liquid;
pub use liquid::{Liquid, LiquidGen, LiquidType};

pub trait ToTerrain {
	fn to_terrain(self) -> Terrain;
	fn name(&self) -> &str;
	fn icon(&self) -> char;
	fn bg(&self) -> Color;
	fn fg(&self) -> Color;
	fn blocks_passage(&self) -> bool;
	// fn blocks_sight(&self) -> bool;
}

#[derive(Clone, Serialize, Deserialize)]
pub enum Terrain {
	Foundation(Foundation),
	// Natural(TerrainNatural),
	// Artificial(TerrainArtificial),
	Liquid(Liquid),
}

impl ToTerrain for Terrain {
	fn to_terrain(self) -> Terrain {
		self
	}

	fn name(&self) -> &str {
		match self {
			Terrain::Foundation(terrain)	=> &terrain.name,
			Terrain::Liquid(liquid)			=> &liquid.name,
		}
	}

	fn icon(&self) -> char {
		match self {
			Terrain::Foundation(terrain)	=> terrain.icon,
			Terrain::Liquid(liquid)			=> liquid.icon,
		}
	}

	fn bg(&self) -> Color {
		match self {
			Terrain::Foundation(terrain)	=> terrain.bg,
			Terrain::Liquid(liquid)			=> liquid.bg,
		}
	}

	fn fg(&self) -> Color {
		match self {
			Terrain::Foundation(terrain)	=> terrain.fg,
			Terrain::Liquid(liquid)			=> liquid.fg,
		}
	}

	fn blocks_passage(&self) -> bool {
		match self {
			Terrain::Foundation(terrain)	=> terrain.blocks_passage,
			// TODO Hmm... Liquids block passage only when walking, but not when swimming.
			Terrain::Liquid(_liquid)			=> true,
		}
	}

	// fn blocks_sight(&self) -> bool {
	// 	match self {
	// 		Terrain::Liquid(liquid) => liquid.blocks_sight,
	// 		Terrain::Liquid(liquid) => liquid.blocks_sight,
	// 	}
	// }
}

pub struct TerrainBundle<'a> {
	pub foundation_gen: FoundationGen<'a>,
	pub liquid_gen: LiquidGen<'a>,
}

impl<'a> Default for TerrainBundle<'a> {
	fn default() -> TerrainBundle<'a> {
		TerrainBundle {
			foundation_gen: FoundationGen::generate(),
			liquid_gen: LiquidGen::generate(),
		}
	}
}

impl<'a> TerrainBundle<'a> {
	pub fn gen_terrain(&self, code: &str) -> Terrain {
		if self.foundation_gen.contains(code) {
			self.foundation_gen.get(code).to_terrain()
		}
		else if self.liquid_gen.contains(code) {
			self.liquid_gen.get(code).to_terrain()
		}
		else {
			panic!("code {} not found", code)
		}
	}

	pub fn gen_foundation(&self, code: &str) -> Foundation {
		self.foundation_gen.get(code)
	}

	pub fn gen_liquid(&self, code: &str) -> Liquid {
		self.liquid_gen.get(code)
	}
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum TerrainType {
	Foundation(foundation::FoundationType),
	Liquid(liquid::LiquidType),
}

pub trait TerrainTypeGenerator {
	type TerrainType;

	fn name_to_type(&self, name: &str) -> Self::TerrainType;
	fn terrain(&self, terrain_type: &Self::TerrainType) -> &Terrain;
}

impl<'a> TerrainTypeGenerator for TerrainBundle<'a> {
	type TerrainType = TerrainType;

	fn name_to_type(&self, name: &str) -> Self::TerrainType {
		if self.foundation_gen.contains(name) {
			TerrainType::Foundation(self.foundation_gen.name_to_type(name))
		} else if self.liquid_gen.contains(name) {
			TerrainType::Liquid(self.liquid_gen.name_to_type(name))
		} else {
			unimplemented!()
		}
	}

	fn terrain(&self, terrain_type: &Self::TerrainType) -> &Terrain {
		match terrain_type {
			TerrainType::Foundation(foundation_type) =>
				self.foundation_gen.terrain(foundation_type)
			,
			TerrainType::Liquid(liquid_type) =>
				self.liquid_gen.terrain(liquid_type)
		}
	}
}