pub struct Fluid<'a> {
	name: &'a str,
	icon: char,
	color: Color,

	level: u8,
}